/*
Navicat MySQL Data Transfer

Source Server         : teantalk
Source Server Version : 50541
Source Host           : 172.16.211.60:3306
Source Database       : teamtalk

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2015-08-28 11:07:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for IMAccess
-- ----------------------------
DROP TABLE IF EXISTS `IMAccess`;
CREATE TABLE `IMAccess` (
  `role_id` smallint(6) unsigned NOT NULL,
  `node_id` smallint(6) unsigned NOT NULL,
  `level` tinyint(1) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module` varchar(50) DEFAULT NULL,
  KEY `groupId` (`role_id`),
  KEY `nodeId` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAccess
-- ----------------------------
INSERT INTO `IMAccess` VALUES ('2', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('2', '84', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('2', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('3', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('1', '125', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('3', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '49', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('4', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('4', '2', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '3', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '4', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '5', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '6', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('4', '11', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('5', '25', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('5', '51', '2', '25', null);
INSERT INTO `IMAccess` VALUES ('1', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('1', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '114', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '90', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '84', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '49', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('3', '69', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('3', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '37', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '36', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '35', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '34', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '33', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '32', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('1', '31', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '32', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('2', '31', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('7', '203', '3', '202', null);
INSERT INTO `IMAccess` VALUES ('1', '113', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '112', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '99', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '2', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('1', '111', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '90', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '114', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '202', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('2', '125', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('7', '125', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('7', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('7', '40', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '49', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('7', '39', '3', '30', null);
INSERT INTO `IMAccess` VALUES ('7', '30', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '7', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '6', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '2', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('7', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('1', '50', '3', '40', null);
INSERT INTO `IMAccess` VALUES ('13', '1', '1', '0', null);
INSERT INTO `IMAccess` VALUES ('13', '84', '2', '1', null);
INSERT INTO `IMAccess` VALUES ('13', '209', '3', '84', null);

-- ----------------------------
-- Table structure for IMAdminUser
-- ----------------------------
DROP TABLE IF EXISTS `IMAdminUser`;
CREATE TABLE `IMAdminUser` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `account` varchar(64) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `bind_account` varchar(50) NOT NULL,
  `last_login_time` int(11) unsigned DEFAULT '0',
  `last_login_ip` varchar(40) DEFAULT NULL,
  `login_count` mediumint(8) unsigned DEFAULT '0',
  `verify` varchar(32) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  `type_id` tinyint(2) unsigned DEFAULT '0',
  `info` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAdminUser
-- ----------------------------
INSERT INTO `IMAdminUser` VALUES ('1', 'admin', '管理员', 'd743e5e5860400b776e2f74a03049f40', '', '1440599972', '119.136.183.33', '1510', '8888', 'luxingzhan@sina.com', '备注信息sss', '1222907803', '1359281644', '1', '0', '');
INSERT INTO `IMAdminUser` VALUES ('43', 'nnn1213', '12313', '8f9cf3f5789e16124f38936954a98668', '', '0', null, '0', null, 'xxx@qq.com', '必要的用户备注', '1439442794', '0', '1', '0', '');

-- ----------------------------
-- Table structure for IMAudio
-- ----------------------------
DROP TABLE IF EXISTS `IMAudio`;
CREATE TABLE `IMAudio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fromId` int(11) unsigned NOT NULL COMMENT '发送者Id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收者Id',
  `path` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '语音存储的地址',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `duration` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '语音时长',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_fromId_toId` (`fromId`,`toId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMAudio
-- ----------------------------
INSERT INTO `IMAudio` VALUES ('1', '13', '12', 'http://msfs.tt.mogujie.org/g0/000/004/1439474798191185_139737244964.audio', '2718', '3', '1439474789');
INSERT INTO `IMAudio` VALUES ('2', '17', '12', 'http://msfs.tt.mogujie.org/g0/000/004/1439540637695762_139737244964.audio', '3621', '4', '1439540628');
INSERT INTO `IMAudio` VALUES ('3', '20', '19', 'http://msfs.tt.mogujie.org/g0/000/004/1439544307771182_139737244964.audio', '5517', '6', '1439544298');
INSERT INTO `IMAudio` VALUES ('4', '30', '12', 'http://msfs.tt.mogujie.org/g0/000/004/1439889191780762_139737244964.audio', '2214', '2', '1439889181');
INSERT INTO `IMAudio` VALUES ('5', '32', '31', 'http://msfs.tt.mogujie.org/g0/000/004/1439947286992837_139737244964.audio', '1689', '2', '1439947276');
INSERT INTO `IMAudio` VALUES ('6', '37', '10', 'http://msfs.tt.mogujie.org/g0/000/005/1439973899672217_139737244964.audio', '2382', '3', '1439973888');
INSERT INTO `IMAudio` VALUES ('7', '29', '28', 'http://msfs.tt.mogujie.org/g0/000/005/1440346579460430_139737244964.audio', '4125', '4', '1440346567');
INSERT INTO `IMAudio` VALUES ('8', '19', '20', 'http://msfs.tt.mogujie.org/g0/000/005/1440393309684248_139737244964.audio', '2550', '3', '1440393297');
INSERT INTO `IMAudio` VALUES ('9', '19', '20', 'http://msfs.tt.mogujie.org/g0/000/005/1440393332107388_139737244964.audio', '1647', '2', '1440393320');
INSERT INTO `IMAudio` VALUES ('10', '19', '20', 'http://msfs.tt.mogujie.org/g0/000/005/1440393338354575_139737244964.audio', '2739', '3', '1440393326');
INSERT INTO `IMAudio` VALUES ('11', '20', '19', 'http://msfs.tt.mogujie.org/g0/000/005/1440393357858907_139737244964.audio', '1878', '2', '1440393345');
INSERT INTO `IMAudio` VALUES ('12', '40', '11', 'http://msfs.tt.mogujie.org/g0/000/005/1440403399925954_139737244964.audio', '2277', '2', '1440403387');
INSERT INTO `IMAudio` VALUES ('13', '20', '11', 'http://msfs.tt.mogujie.org/g0/000/005/1440404968198768_139737244964.audio', '2235', '2', '1440404954');
INSERT INTO `IMAudio` VALUES ('14', '41', '34', 'http://msfs.tt.mogujie.org/g0/000/005/1440409647816226_139737244964.audio', '1248', '1', '1440409635');
INSERT INTO `IMAudio` VALUES ('15', '41', '37', 'http://msfs.tt.mogujie.org/g0/000/005/1440410386892049_139737244964.audio', '2823', '2', '1440410374');
INSERT INTO `IMAudio` VALUES ('16', '49', '33', 'http://msfs.tt.mogujie.org/g0/000/005/1440422627140647_139737244964.audio', '2277', '2', '1440422614');
INSERT INTO `IMAudio` VALUES ('17', '51', '12', 'http://msfs.tt.mogujie.org/g0/000/005/1440478805135861_139737244964.audio', '1878', '2', '1440478792');
INSERT INTO `IMAudio` VALUES ('18', '56', '3', 'http://msfs.tt.mogujie.org/g0/000/005/1440491674572060_139737244964.audio', '171', '4', '1440491662');
INSERT INTO `IMAudio` VALUES ('19', '59', '28', 'http://msfs.tt.mogujie.org/g0/000/004/1440577626384033_140649143371.audio', '5895', '6', '1440577613');

-- ----------------------------
-- Table structure for IMAuthGroup
-- ----------------------------
DROP TABLE IF EXISTS `IMAuthGroup`;
CREATE TABLE `IMAuthGroup` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAuthGroup
-- ----------------------------
INSERT INTO `IMAuthGroup` VALUES ('6', '旗舰店', '');
INSERT INTO `IMAuthGroup` VALUES ('7', '出版社', '');
INSERT INTO `IMAuthGroup` VALUES ('8', '图书管理员', '7,8,9,10,11,12');

-- ----------------------------
-- Table structure for IMAuthGroupAccess
-- ----------------------------
DROP TABLE IF EXISTS `IMAuthGroupAccess`;
CREATE TABLE `IMAuthGroupAccess` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_2` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAuthGroupAccess
-- ----------------------------
INSERT INTO `IMAuthGroupAccess` VALUES ('1', '7');
INSERT INTO `IMAuthGroupAccess` VALUES ('2', '6');
INSERT INTO `IMAuthGroupAccess` VALUES ('3', '6');

-- ----------------------------
-- Table structure for IMAuthRule
-- ----------------------------
DROP TABLE IF EXISTS `IMAuthRule`;
CREATE TABLE `IMAuthRule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '',
  `title` char(40) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `condition` char(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMAuthRule
-- ----------------------------
INSERT INTO `IMAuthRule` VALUES ('6', 'book_upload', '图书上传权限', '0', '');
INSERT INTO `IMAuthRule` VALUES ('7', 'edit_any', '可编辑他人数据', '0', '');
INSERT INTO `IMAuthRule` VALUES ('8', 'admin_order', '管理用户订单', '0', '');
INSERT INTO `IMAuthRule` VALUES ('9', 'admin_book', '管理图书', '0', '');
INSERT INTO `IMAuthRule` VALUES ('10', 'edit_shop', '编辑店铺信息', '0', '');
INSERT INTO `IMAuthRule` VALUES ('11', 'beihuo', '备货权限', '0', '');
INSERT INTO `IMAuthRule` VALUES ('12', 'admin_beihuo', '管理备货订单', '0', '');

-- ----------------------------
-- Table structure for IMDepart
-- ----------------------------
DROP TABLE IF EXISTS `IMDepart`;
CREATE TABLE `IMDepart` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `departName` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '部门名称',
  `priority` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '显示优先级',
  `parentId` int(11) unsigned NOT NULL COMMENT '上级部门id',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_departName` (`departName`),
  KEY `idx_priority_status` (`priority`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMDepart
-- ----------------------------
INSERT INTO `IMDepart` VALUES ('1', '默认注册部门', '0', '0', '0', '1439370664', '1439370664');
INSERT INTO `IMDepart` VALUES ('2', '测试注册部门小弟部门', '0', '1', '0', '1439370664', '1439370664');

-- ----------------------------
-- Table structure for IMDiscovery
-- ----------------------------
DROP TABLE IF EXISTS `IMDiscovery`;
CREATE TABLE `IMDiscovery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `itemName` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '名称',
  `itemUrl` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'URL',
  `itemPriority` int(11) unsigned NOT NULL COMMENT '显示优先级',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_itemName` (`itemName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMDiscovery
-- ----------------------------
INSERT INTO `IMDiscovery` VALUES ('2', 'sssss', 'http://www.baidu.com', '1', '0', '0', '0');
INSERT INTO `IMDiscovery` VALUES ('3', 'ccccc', 'http://www.baidu.com', '2', '0', '0', '0');
INSERT INTO `IMDiscovery` VALUES ('4', 'bbbb', 'http://www.baidu.com', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for IMGroup
-- ----------------------------
DROP TABLE IF EXISTS `IMGroup`;
CREATE TABLE `IMGroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '群名称',
  `avatar` varchar(256) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '群头像',
  `creator` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建者用户id',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '群组类型，1-固定;2-临时群',
  `userCnt` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '成员人数',
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否删除,0-正常，1-删除',
  `version` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '群版本号',
  `lastChated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后聊天时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`(191)),
  KEY `idx_creator` (`creator`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群信息';

-- ----------------------------
-- Records of IMGroup
-- ----------------------------
INSERT INTO `IMGroup` VALUES ('1', '群', '', '6', '2', '6', '0', '1', '1439390514', '1439390136', '1439390136');
INSERT INTO `IMGroup` VALUES ('2', 'test', '', '30', '2', '8', '0', '1', '1439889518', '1439889460', '1439889460');
INSERT INTO `IMGroup` VALUES ('3', '风格哈', '', '13', '2', '6', '0', '1', '1439910927', '1439910919', '1439910919');
INSERT INTO `IMGroup` VALUES ('4', '几点', '', '13', '2', '6', '0', '1', '1439911444', '1439911441', '1439911441');
INSERT INTO `IMGroup` VALUES ('5', '地方', '', '13', '2', '6', '0', '1', '1439911993', '1439911916', '1439911916');
INSERT INTO `IMGroup` VALUES ('6', '出差', '', '13', '2', '6', '0', '1', '1439912015', '1439911947', '1439911947');
INSERT INTO `IMGroup` VALUES ('7', 'hhg', '', '32', '2', '8', '0', '1', '1439947883', '1439946779', '1439946779');
INSERT INTO `IMGroup` VALUES ('8', '奋斗奋斗', '', '27', '2', '4', '0', '1', '1439972342', '1439972274', '1439972274');
INSERT INTO `IMGroup` VALUES ('9', '54t645', '', '28', '2', '4', '0', '1', '1439973576', '1439973154', '1439973154');
INSERT INTO `IMGroup` VALUES ('10', '州官', '', '37', '2', '10', '0', '3', '1439978554', '1439973713', '1439973713');
INSERT INTO `IMGroup` VALUES ('11', 'test', '', '40', '2', '6', '0', '1', '1440404957', '1440402310', '1440402310');
INSERT INTO `IMGroup` VALUES ('12', '刚刚好哈哈哈', '', '51', '2', '12', '0', '1', '1440478876', '1440478871', '1440478871');

-- ----------------------------
-- Table structure for IMGroupAdmin
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupAdmin`;
CREATE TABLE `IMGroupAdmin` (
  `id` smallint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `title` varchar(50) NOT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` smallint(3) unsigned NOT NULL DEFAULT '0',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_menu` char(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroupAdmin
-- ----------------------------
INSERT INTO `IMGroupAdmin` VALUES ('9', 'webapp', '用户数据管理', '1316267260', '0', '1', '2', '0', 'About');
INSERT INTO `IMGroupAdmin` VALUES ('2', 'System', '系统设置', '1222841259', '0', '1', '20', '0', 'info');
INSERT INTO `IMGroupAdmin` VALUES ('24', 'News', '信息管理', '1375687191', '0', '1', '0', '0', 'News');
INSERT INTO `IMGroupAdmin` VALUES ('29', 'Sysconfig', '核心系统配置', '1437484752', '0', '1', '0', '0', 'info');

-- ----------------------------
-- Table structure for IMGroupClass
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupClass`;
CREATE TABLE `IMGroupClass` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu` char(10) NOT NULL DEFAULT '0',
  `name` char(25) NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `sort` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroupClass
-- ----------------------------
INSERT INTO `IMGroupClass` VALUES ('1', 'info', '系统管理', '1', '100');
INSERT INTO `IMGroupClass` VALUES ('20', 'News', '其他管理', '1', '100');
INSERT INTO `IMGroupClass` VALUES ('21', 'About', '用户管理', '1', '100');

-- ----------------------------
-- Table structure for IMGroupClassUser
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupClassUser`;
CREATE TABLE `IMGroupClassUser` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `gc_id` int(6) DEFAULT NULL,
  `uid` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `gc` (`gc_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroupClassUser
-- ----------------------------
INSERT INTO `IMGroupClassUser` VALUES ('28', '3', '2');
INSERT INTO `IMGroupClassUser` VALUES ('29', '3', '3');
INSERT INTO `IMGroupClassUser` VALUES ('30', '3', '4');
INSERT INTO `IMGroupClassUser` VALUES ('7', '1', '1');
INSERT INTO `IMGroupClassUser` VALUES ('8', '1', '2');
INSERT INTO `IMGroupClassUser` VALUES ('9', '1', '3');
INSERT INTO `IMGroupClassUser` VALUES ('10', '1', '4');
INSERT INTO `IMGroupClassUser` VALUES ('11', '1', '38');
INSERT INTO `IMGroupClassUser` VALUES ('12', '1', '39');
INSERT INTO `IMGroupClassUser` VALUES ('13', '2', '1');
INSERT INTO `IMGroupClassUser` VALUES ('15', '2', '3');
INSERT INTO `IMGroupClassUser` VALUES ('16', '2', '4');
INSERT INTO `IMGroupClassUser` VALUES ('17', '2', '38');
INSERT INTO `IMGroupClassUser` VALUES ('18', '2', '39');
INSERT INTO `IMGroupClassUser` VALUES ('19', '5', '1');
INSERT INTO `IMGroupClassUser` VALUES ('20', '5', '2');
INSERT INTO `IMGroupClassUser` VALUES ('21', '5', '3');
INSERT INTO `IMGroupClassUser` VALUES ('22', '5', '4');
INSERT INTO `IMGroupClassUser` VALUES ('23', '5', '38');
INSERT INTO `IMGroupClassUser` VALUES ('24', '5', '39');
INSERT INTO `IMGroupClassUser` VALUES ('31', '3', '38');

-- ----------------------------
-- Table structure for IMGroupMember
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMember`;
CREATE TABLE `IMGroupMember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '群Id',
  `userId` int(11) unsigned NOT NULL COMMENT '用户id',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1' COMMENT '是否退出群，0-正常，1-已退出',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_userId_status` (`groupId`,`userId`,`status`),
  KEY `idx_userId_status_updated` (`userId`,`status`,`updated`),
  KEY `idx_groupId_updated` (`groupId`,`updated`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='用户和群的关系表';

-- ----------------------------
-- Records of IMGroupMember
-- ----------------------------
INSERT INTO `IMGroupMember` VALUES ('1', '1', '2', '0', '1439390136', '1439390136');
INSERT INTO `IMGroupMember` VALUES ('2', '1', '3', '0', '1439390136', '1439390136');
INSERT INTO `IMGroupMember` VALUES ('3', '1', '6', '0', '1439390136', '1439390136');
INSERT INTO `IMGroupMember` VALUES ('4', '2', '12', '0', '1439889460', '1439889460');
INSERT INTO `IMGroupMember` VALUES ('5', '2', '18', '0', '1439889460', '1439889460');
INSERT INTO `IMGroupMember` VALUES ('6', '2', '24', '0', '1439889460', '1439889460');
INSERT INTO `IMGroupMember` VALUES ('7', '2', '30', '0', '1439889460', '1439889460');
INSERT INTO `IMGroupMember` VALUES ('8', '3', '6', '0', '1439910919', '1439910919');
INSERT INTO `IMGroupMember` VALUES ('9', '3', '12', '0', '1439910919', '1439910919');
INSERT INTO `IMGroupMember` VALUES ('10', '3', '13', '0', '1439910919', '1439910919');
INSERT INTO `IMGroupMember` VALUES ('11', '4', '3', '0', '1439911441', '1439911441');
INSERT INTO `IMGroupMember` VALUES ('12', '4', '6', '0', '1439911441', '1439911441');
INSERT INTO `IMGroupMember` VALUES ('13', '4', '13', '0', '1439911441', '1439911441');
INSERT INTO `IMGroupMember` VALUES ('14', '5', '9', '0', '1439911916', '1439911916');
INSERT INTO `IMGroupMember` VALUES ('15', '5', '13', '0', '1439911916', '1439911916');
INSERT INTO `IMGroupMember` VALUES ('16', '5', '14', '0', '1439911916', '1439911916');
INSERT INTO `IMGroupMember` VALUES ('17', '6', '9', '0', '1439911947', '1439911947');
INSERT INTO `IMGroupMember` VALUES ('18', '6', '13', '0', '1439911947', '1439911947');
INSERT INTO `IMGroupMember` VALUES ('19', '6', '15', '0', '1439911947', '1439911947');
INSERT INTO `IMGroupMember` VALUES ('20', '7', '9', '0', '1439946779', '1439946779');
INSERT INTO `IMGroupMember` VALUES ('21', '7', '21', '0', '1439946779', '1439946779');
INSERT INTO `IMGroupMember` VALUES ('22', '7', '22', '0', '1439946779', '1439946779');
INSERT INTO `IMGroupMember` VALUES ('23', '7', '32', '0', '1439946779', '1439946779');
INSERT INTO `IMGroupMember` VALUES ('24', '8', '27', '0', '1439972274', '1439972274');
INSERT INTO `IMGroupMember` VALUES ('25', '8', '35', '0', '1439972274', '1439972274');
INSERT INTO `IMGroupMember` VALUES ('26', '9', '28', '0', '1439973154', '1439973154');
INSERT INTO `IMGroupMember` VALUES ('27', '9', '35', '0', '1439973154', '1439973154');
INSERT INTO `IMGroupMember` VALUES ('28', '10', '27', '0', '1439973713', '1439973713');
INSERT INTO `IMGroupMember` VALUES ('29', '10', '34', '0', '1439973713', '1439973713');
INSERT INTO `IMGroupMember` VALUES ('30', '10', '35', '0', '1439973713', '1439973713');
INSERT INTO `IMGroupMember` VALUES ('31', '10', '37', '0', '1439973713', '1439973713');
INSERT INTO `IMGroupMember` VALUES ('32', '10', '28', '0', '1439973742', '1439973742');
INSERT INTO `IMGroupMember` VALUES ('33', '10', '30', '0', '1439973802', '1439973802');
INSERT INTO `IMGroupMember` VALUES ('34', '11', '19', '0', '1440402310', '1440402310');
INSERT INTO `IMGroupMember` VALUES ('35', '11', '20', '0', '1440402310', '1440402310');
INSERT INTO `IMGroupMember` VALUES ('36', '11', '40', '0', '1440402310', '1440402310');
INSERT INTO `IMGroupMember` VALUES ('37', '12', '18', '0', '1440478871', '1440478871');
INSERT INTO `IMGroupMember` VALUES ('38', '12', '43', '0', '1440478871', '1440478871');
INSERT INTO `IMGroupMember` VALUES ('39', '12', '46', '0', '1440478871', '1440478871');
INSERT INTO `IMGroupMember` VALUES ('40', '12', '47', '0', '1440478871', '1440478871');
INSERT INTO `IMGroupMember` VALUES ('41', '12', '50', '0', '1440478871', '1440478871');
INSERT INTO `IMGroupMember` VALUES ('42', '12', '51', '0', '1440478871', '1440478871');

-- ----------------------------
-- Table structure for IMGroupMessage_0
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_0`;
CREATE TABLE `IMGroupMessage_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_0
-- ----------------------------
INSERT INTO `IMGroupMessage_0` VALUES ('1', '8', '27', '1', '3HwXri5Y+07Wvfb9hshsPQ==', '17', '0', '1439972288', '1439972288');
INSERT INTO `IMGroupMessage_0` VALUES ('2', '8', '27', '2', 'TiaXTHkojG6RftDuWUIDzNOuD9XcHAxYCdfJhFulpUU=', '17', '0', '1439972307', '1439972307');
INSERT INTO `IMGroupMessage_0` VALUES ('3', '8', '27', '3', 'NrJp/PIVXgsbuKRPPnGqH5xNU+fSaAo3m5PqGE4eDg4=', '17', '0', '1439972342', '1439972342');

-- ----------------------------
-- Table structure for IMGroupMessage_1
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_1`;
CREATE TABLE `IMGroupMessage_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_1
-- ----------------------------
INSERT INTO `IMGroupMessage_1` VALUES ('1', '1', '6', '1', 'gkELJinpTMWnofK9zgPoNnWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439390139', '1439390139');
INSERT INTO `IMGroupMessage_1` VALUES ('2', '1', '2', '2', '6qx7MXHwPo+gi42UIK3Fgw==', '17', '0', '1439390166', '1439390166');
INSERT INTO `IMGroupMessage_1` VALUES ('3', '1', '6', '3', 'EYgp9iR11FlzZnX1OT/QLJ3ganwyAOnkp6GqwVnHDdg=', '17', '0', '1439390192', '1439390192');
INSERT INTO `IMGroupMessage_1` VALUES ('4', '1', '2', '4', 'l79cQNZD/JmD1Lz3Q/XWkyW2X6lmVYORJ9etn0IrwClIapqqEpcbQFG38HGZvS9D', '17', '0', '1439390214', '1439390214');
INSERT INTO `IMGroupMessage_1` VALUES ('5', '1', '6', '5', 'zTAz3RzZj4zWLRSJLAlDB2cYweFVs4X/sKy8uCvircs=', '17', '0', '1439390249', '1439390249');
INSERT INTO `IMGroupMessage_1` VALUES ('6', '1', '2', '6', '9ze0Y+5P/7bd50EDK00bpw==', '17', '0', '1439390277', '1439390277');
INSERT INTO `IMGroupMessage_1` VALUES ('7', '1', '6', '7', 'QKzq/9e8SYq8BLrNj/NOujX+UNehHs5alDVOSdqzrP7WcL+S56WeVJsnw974ehXNrF1ZRq04hpBraEeYfBkKkA==', '17', '0', '1439390333', '1439390333');
INSERT INTO `IMGroupMessage_1` VALUES ('8', '1', '6', '8', 'xxTuH9dMQzf0MEY/BK4O4pWHx8vWGOC847FuCBUstP0=', '17', '0', '1439390345', '1439390345');
INSERT INTO `IMGroupMessage_1` VALUES ('9', '1', '2', '9', 'NeR0NueVp0oNMXkb6kBkBYa49sYrZnYwS1NkYOA5Ye0=', '17', '0', '1439390369', '1439390369');
INSERT INTO `IMGroupMessage_1` VALUES ('10', '1', '6', '10', 'N9KwsoUxWrI/BKQ7I4ilbw==', '17', '0', '1439390383', '1439390383');
INSERT INTO `IMGroupMessage_1` VALUES ('11', '1', '2', '11', 'CTRUCT8JRvkZB3NOcb3q8KjEl4Opdjj4OG1ij7RFDyd7QOX8mT9gZZPrc/YZNt9si5IiB4/iqeMkjIb9pplEwSqY7u5n8QTKbhP8tRWvq3zs99ZazTMMX56fF5qDkbSW', '17', '0', '1439390405', '1439390405');
INSERT INTO `IMGroupMessage_1` VALUES ('12', '1', '6', '12', 'kNUiU68dbqKaRdQ40+05s7XvKQ2k5/LyMUGoKrFWLGYkowxj9A2z83U1R3s+aJ8wT981gYu3xcfRXOJLVsO/czwuHcaWcrNjTubX8VWvZmdS0AHzbqOopRyekzR0sLBa', '17', '0', '1439390472', '1439390472');
INSERT INTO `IMGroupMessage_1` VALUES ('13', '1', '2', '13', 'yq7MQoCfGo8qZOiop8E2KamFbM7vlu6cuB99FNUJMaAA+3WJKCKuK4a+DrYZUXfz', '17', '0', '1439390503', '1439390503');
INSERT INTO `IMGroupMessage_1` VALUES ('14', '1', '6', '14', 'fA+Y5lPm8EF3HeNrYIYa6g==', '17', '0', '1439390511', '1439390511');
INSERT INTO `IMGroupMessage_1` VALUES ('15', '1', '2', '15', 'Ex5wGtvjB34RK+4brUytKWTOSlz7HrrlOSwpwMv/Oao=', '17', '0', '1439390514', '1439390514');
INSERT INTO `IMGroupMessage_1` VALUES ('16', '9', '28', '1', '0bNNloOAfyKoKsG/PxDYlg==', '17', '0', '1439973430', '1439973430');
INSERT INTO `IMGroupMessage_1` VALUES ('17', '9', '28', '2', 'VXjm7p2JeJnYHQCdF+BE3nWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439973433', '1439973433');
INSERT INTO `IMGroupMessage_1` VALUES ('18', '9', '35', '3', 'DBhrqmmfLJSbEMFlatBstQ==', '17', '0', '1439973576', '1439973576');

-- ----------------------------
-- Table structure for IMGroupMessage_2
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_2`;
CREATE TABLE `IMGroupMessage_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_2
-- ----------------------------
INSERT INTO `IMGroupMessage_2` VALUES ('1', '2', '30', '1', 'rJ/qmU7eoa7r6E7LqeBh8g==', '17', '0', '1439889518', '1439889518');
INSERT INTO `IMGroupMessage_2` VALUES ('2', '10', '37', '1', 'yPi1udbJkJvn+R1KCyeZuXWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439973717', '1439973717');
INSERT INTO `IMGroupMessage_2` VALUES ('3', '10', '37', '2', 'B1noWBnVZfBNEV48VQiRvg==', '17', '0', '1439973725', '1439973725');
INSERT INTO `IMGroupMessage_2` VALUES ('4', '10', '37', '3', 'hBg5P6tthdsw5J+6Ze6vvQ==', '17', '0', '1439973753', '1439973753');
INSERT INTO `IMGroupMessage_2` VALUES ('5', '10', '28', '4', 'LBzqhrj1CJzgIfCNOdENvA==', '17', '0', '1439973825', '1439973825');
INSERT INTO `IMGroupMessage_2` VALUES ('6', '10', '37', '5', 'nnZSBwfPCz60q69ddmfIJA==', '17', '0', '1439973825', '1439973825');
INSERT INTO `IMGroupMessage_2` VALUES ('7', '10', '28', '6', 'Oi9uYQggKzQOyt/ws6kcZXWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439973838', '1439973838');
INSERT INTO `IMGroupMessage_2` VALUES ('8', '10', '37', '7', '6', '18', '0', '1439973888', '1439973888');
INSERT INTO `IMGroupMessage_2` VALUES ('9', '10', '37', '8', 'b8iDWP8x2TghweC3yneT7Q==', '17', '0', '1439974050', '1439974050');
INSERT INTO `IMGroupMessage_2` VALUES ('10', '10', '28', '9', 'YWpT8JdQ3luDpvK1xM/M7Q==', '17', '0', '1439974050', '1439974050');
INSERT INTO `IMGroupMessage_2` VALUES ('11', '10', '28', '10', 'eIVJ1E0lecmF2uO6Iieo3+i/MYD5suMXlN57FuVsHh8=', '17', '0', '1439974060', '1439974060');
INSERT INTO `IMGroupMessage_2` VALUES ('12', '10', '37', '11', 'OD2LbeuSaVDljpRAHkZpyQ==', '17', '0', '1439974087', '1439974087');
INSERT INTO `IMGroupMessage_2` VALUES ('13', '10', '37', '12', 'Z3CFOmwtlIy8QeVZG8C1Jg==', '17', '0', '1439974099', '1439974099');
INSERT INTO `IMGroupMessage_2` VALUES ('14', '10', '37', '13', 'aD75I8yB6Abk2u3UxEW45Q==', '17', '0', '1439974108', '1439974108');
INSERT INTO `IMGroupMessage_2` VALUES ('15', '10', '37', '14', 'YLiTYvi4tom4ZenOLQa24A==', '17', '0', '1439974121', '1439974121');
INSERT INTO `IMGroupMessage_2` VALUES ('16', '10', '37', '15', 'qhNqCFdnwq2CTnAZHFBZKHWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439974144', '1439974144');
INSERT INTO `IMGroupMessage_2` VALUES ('17', '10', '37', '16', 'aD75I8yB6Abk2u3UxEW45Q==', '17', '0', '1439974155', '1439974155');
INSERT INTO `IMGroupMessage_2` VALUES ('18', '10', '28', '17', 'grkVipLOHc6OSo5JEw+/T7xgoyyGeEe8ehW5vmHwi0wBDyeuSoWNnW4SMeB8UwyL', '17', '0', '1439974183', '1439974183');
INSERT INTO `IMGroupMessage_2` VALUES ('19', '10', '37', '18', 'r1OmfBiueQ2d3xFUS+1BkA==', '17', '0', '1439974276', '1439974276');
INSERT INTO `IMGroupMessage_2` VALUES ('20', '10', '37', '19', 'p85aMEh5ZITwSmPzANMmlQ==', '17', '0', '1439978554', '1439978554');

-- ----------------------------
-- Table structure for IMGroupMessage_3
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_3`;
CREATE TABLE `IMGroupMessage_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_3
-- ----------------------------
INSERT INTO `IMGroupMessage_3` VALUES ('1', '3', '13', '1', 'LN5ffg41UHrHpI7fIMIAGA==', '17', '0', '1439910924', '1439910924');
INSERT INTO `IMGroupMessage_3` VALUES ('2', '3', '13', '2', 'wd/9pR7Weftcd16PlqztDw==', '17', '0', '1439910927', '1439910927');
INSERT INTO `IMGroupMessage_3` VALUES ('3', '11', '40', '1', 'B+HuUVixIt9S2wq8XgAH9A==', '17', '0', '1440402348', '1440402348');
INSERT INTO `IMGroupMessage_3` VALUES ('4', '11', '19', '2', 'UKheN9KmjZEOcj7ZdGedvw==', '17', '0', '1440402356', '1440402356');
INSERT INTO `IMGroupMessage_3` VALUES ('5', '11', '19', '3', 'KLjukTD9hE6+v4LTCu7H8nVwZyDc3Pdt77VihzoiEpillyJt6TWlSi+HC0PvMgEk', '17', '0', '1440402369', '1440402369');
INSERT INTO `IMGroupMessage_3` VALUES ('6', '11', '40', '4', 'p0tOR9muV+pM11nhpftF+Om+f5QvkewlZ5SwOXWc4Jg=', '17', '0', '1440402391', '1440402391');
INSERT INTO `IMGroupMessage_3` VALUES ('7', '11', '19', '5', 'ZBeq57EXwTyaP4JqzBNAAsg45XXvdSBa3ST38JlczohvjFdFIEc983B/37Tqcpsn', '17', '0', '1440402813', '1440402813');
INSERT INTO `IMGroupMessage_3` VALUES ('8', '11', '40', '6', 'Su+3CszXHShH0QWWTaRn/Ar+GZ43+2mMfQMFqTsGBjY=', '17', '0', '1440402836', '1440402836');
INSERT INTO `IMGroupMessage_3` VALUES ('9', '11', '19', '7', 'c9lC9aOR1DeBF41Ft9EfbG/Vm6T7Rk2OnG5+Up61ccRtBcSGbgefwpOm6lZseoEs', '17', '0', '1440403348', '1440403348');
INSERT INTO `IMGroupMessage_3` VALUES ('10', '11', '40', '8', 'BkMtzhxMCWRe5bP71dFuIQ==', '17', '0', '1440403370', '1440403370');
INSERT INTO `IMGroupMessage_3` VALUES ('11', '11', '40', '9', 'yPi1udbJkJvn+R1KCyeZuXWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1440403375', '1440403375');
INSERT INTO `IMGroupMessage_3` VALUES ('12', '11', '40', '10', '12', '18', '0', '1440403387', '1440403387');
INSERT INTO `IMGroupMessage_3` VALUES ('13', '11', '19', '11', 'dscnnibYAHCkpQenMd87GQ==', '17', '0', '1440403393', '1440403393');
INSERT INTO `IMGroupMessage_3` VALUES ('14', '11', '20', '12', '13', '18', '0', '1440404954', '1440404954');

-- ----------------------------
-- Table structure for IMGroupMessage_4
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_4`;
CREATE TABLE `IMGroupMessage_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_4
-- ----------------------------
INSERT INTO `IMGroupMessage_4` VALUES ('1', '4', '13', '1', 'e2P29FOIpXXiQwy0VBk/oA==', '17', '0', '1439911444', '1439911444');
INSERT INTO `IMGroupMessage_4` VALUES ('2', '12', '51', '1', 'XVsRHPsLnZLwAiUDhnCoPw==', '17', '0', '1440478876', '1440478876');

-- ----------------------------
-- Table structure for IMGroupMessage_5
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_5`;
CREATE TABLE `IMGroupMessage_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_5
-- ----------------------------
INSERT INTO `IMGroupMessage_5` VALUES ('1', '5', '13', '1', '/BWL1vdgCMGQ/u2lvzcf4g==', '17', '0', '1439911993', '1439911993');

-- ----------------------------
-- Table structure for IMGroupMessage_6
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_6`;
CREATE TABLE `IMGroupMessage_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_6
-- ----------------------------
INSERT INTO `IMGroupMessage_6` VALUES ('1', '6', '13', '1', '/yL//49B+lhyefFMtcX8nQ==', '17', '0', '1439911987', '1439911987');
INSERT INTO `IMGroupMessage_6` VALUES ('2', '6', '13', '2', '4Cv0afTVLTPURQ6b/nSLmA==', '17', '0', '1439912015', '1439912015');

-- ----------------------------
-- Table structure for IMGroupMessage_7
-- ----------------------------
DROP TABLE IF EXISTS `IMGroupMessage_7`;
CREATE TABLE `IMGroupMessage_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `userId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '消息内容',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '群消息类型,101为群语音,2为文本',
  `status` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '消息状态',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_groupId_status_created` (`groupId`,`status`,`created`),
  KEY `idx_groupId_msgId_status_created` (`groupId`,`msgId`,`status`,`created`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='IM群消息表';

-- ----------------------------
-- Records of IMGroupMessage_7
-- ----------------------------
INSERT INTO `IMGroupMessage_7` VALUES ('1', '7', '32', '1', 'fUGIQsxwQJVKb/VRk+mXTg==', '17', '0', '1439947872', '1439947872');
INSERT INTO `IMGroupMessage_7` VALUES ('2', '7', '32', '2', 'bUgBq505vcVhUGuEsx6/vg==', '17', '0', '1439947875', '1439947875');
INSERT INTO `IMGroupMessage_7` VALUES ('3', '7', '32', '3', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439947879', '1439947879');
INSERT INTO `IMGroupMessage_7` VALUES ('4', '7', '32', '4', 'XJGVB14N561q7GFM8q1+U3WXrZFfV+UBo37vcxL/NUY=', '17', '0', '1439947883', '1439947883');

-- ----------------------------
-- Table structure for IMGroups
-- ----------------------------
DROP TABLE IF EXISTS `IMGroups`;
CREATE TABLE `IMGroups` (
  `id` mediumint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMGroups
-- ----------------------------
INSERT INTO `IMGroups` VALUES ('1', '项目组1');
INSERT INTO `IMGroups` VALUES ('2', '项目组2');
INSERT INTO `IMGroups` VALUES ('3', '项目组3');

-- ----------------------------
-- Table structure for IMLog
-- ----------------------------
DROP TABLE IF EXISTS `IMLog`;
CREATE TABLE `IMLog` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vc_module` char(50) DEFAULT NULL,
  `vc_operation` char(200) DEFAULT NULL,
  `creator_id` bigint(20) DEFAULT NULL,
  `creator_name` char(50) NOT NULL,
  `vc_ip` char(50) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `createtime` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=395 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMLog
-- ----------------------------
INSERT INTO `IMLog` VALUES ('193', '系统管理', '用户登录：登录成功！', '1', 'admin', '127.0.0.1', '1', '1357458137');
INSERT INTO `IMLog` VALUES ('350', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438416581');
INSERT INTO `IMLog` VALUES ('351', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438417088');
INSERT INTO `IMLog` VALUES ('352', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438417305');
INSERT INTO `IMLog` VALUES ('353', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438772488');
INSERT INTO `IMLog` VALUES ('354', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1438788029');
INSERT INTO `IMLog` VALUES ('355', '系统管理', '用户登录：登录成功！', '1', 'admin', '58.211.28.178', '1', '1439348140');
INSERT INTO `IMLog` VALUES ('356', '系统管理', '用户登录：登录成功！', '1', 'admin', '121.11.169.94', '1', '1439348154');
INSERT INTO `IMLog` VALUES ('357', '系统管理', '用户登录：登录成功！', '1', 'admin', '113.97.189.44', '1', '1439348155');
INSERT INTO `IMLog` VALUES ('358', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.236.53.50', '1', '1439348175');
INSERT INTO `IMLog` VALUES ('359', '系统管理', '用户登录：登录成功！', '1', 'admin', '58.19.78.176', '1', '1439348217');
INSERT INTO `IMLog` VALUES ('360', '系统管理', '用户登录：登录成功！', '1', 'admin', '116.24.248.48', '1', '1439348334');
INSERT INTO `IMLog` VALUES ('361', '系统管理', '用户登录：登录成功！', '1', 'admin', '183.26.224.151', '1', '1439348352');
INSERT INTO `IMLog` VALUES ('362', '系统管理', '用户登录：登录成功！', '1', 'admin', '182.88.163.65', '1', '1439348362');
INSERT INTO `IMLog` VALUES ('363', '系统管理', '用户登录：登录成功！', '1', 'admin', '106.2.213.42', '1', '1439348365');
INSERT INTO `IMLog` VALUES ('364', '系统管理', '用户登录：登录成功！', '1', 'admin', '113.99.103.47', '1', '1439348393');
INSERT INTO `IMLog` VALUES ('365', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439348465');
INSERT INTO `IMLog` VALUES ('366', '系统管理', '用户登录：登录成功！', '1', 'admin', '121.15.10.33', '1', '1439348707');
INSERT INTO `IMLog` VALUES ('367', '系统管理', '用户登录：登录成功！', '1', 'admin', '222.178.223.118', '1', '1439348716');
INSERT INTO `IMLog` VALUES ('368', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.236.53.50', '1', '1439371744');
INSERT INTO `IMLog` VALUES ('369', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439374476');
INSERT INTO `IMLog` VALUES ('370', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439433276');
INSERT INTO `IMLog` VALUES ('371', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439433594');
INSERT INTO `IMLog` VALUES ('372', '系统管理', '用户登录：登录成功！', '1', 'admin', '0.0.0.0', '1', '1439441103');
INSERT INTO `IMLog` VALUES ('373', '系统管理', '用户登录：登录成功！', '1', 'admin', '0.0.0.0', '1', '1439441240');
INSERT INTO `IMLog` VALUES ('374', '系统管理', '用户登录：登录成功！', '1', 'admin', '0.0.0.0', '1', '1439441480');
INSERT INTO `IMLog` VALUES ('375', '系统管理', '用户登录：登录成功！', '1', 'admin', '0.0.0.0', '1', '1439442635');
INSERT INTO `IMLog` VALUES ('376', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439645195');
INSERT INTO `IMLog` VALUES ('377', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.206.99.227', '1', '1439645251');
INSERT INTO `IMLog` VALUES ('378', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.206.99.227', '1', '1439645917');
INSERT INTO `IMLog` VALUES ('379', '系统管理', '用户登录：登录成功！', '1', 'admin', '122.233.242.12', '1', '1439657408');
INSERT INTO `IMLog` VALUES ('380', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439709617');
INSERT INTO `IMLog` VALUES ('381', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439711881');
INSERT INTO `IMLog` VALUES ('382', '系统管理', '用户登录：登录成功！', '1', 'admin', '175.12.8.242', '1', '1439812248');
INSERT INTO `IMLog` VALUES ('383', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439821548');
INSERT INTO `IMLog` VALUES ('384', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439871916');
INSERT INTO `IMLog` VALUES ('385', '系统管理', '用户登录：登录成功！', '1', 'admin', '119.136.181.238', '1', '1439965352');
INSERT INTO `IMLog` VALUES ('386', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439965440');
INSERT INTO `IMLog` VALUES ('387', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1439991718');
INSERT INTO `IMLog` VALUES ('388', '系统管理', '用户登录：登录成功！', '1', 'admin', '116.25.65.94', '1', '1440122436');
INSERT INTO `IMLog` VALUES ('389', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1440408485');
INSERT INTO `IMLog` VALUES ('390', '系统管理', '用户登录：登录成功！', '1', 'admin', '218.72.90.108', '1', '1440429389');
INSERT INTO `IMLog` VALUES ('391', '系统管理', '用户登录：登录成功！', '1', 'admin', '172.16.1.252', '1', '1440475822');
INSERT INTO `IMLog` VALUES ('392', '系统管理', '用户登录：登录成功！', '1', 'admin', '115.236.53.50', '1', '1440486071');
INSERT INTO `IMLog` VALUES ('393', '系统管理', '用户登录：登录成功！', '1', 'admin', '218.72.90.108', '1', '1440491822');
INSERT INTO `IMLog` VALUES ('394', '系统管理', '用户登录：登录成功！', '1', 'admin', '119.136.183.33', '1', '1440599972');

-- ----------------------------
-- Table structure for IMMessage_0
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_0`;
CREATE TABLE `IMMessage_0` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_0
-- ----------------------------
INSERT INTO `IMMessage_0` VALUES ('1', '8', '30', '12', '1', 'NEr8TKwoMSUYc2NjSiIAEw==', '1', '0', '1439889147', '1439889147');
INSERT INTO `IMMessage_0` VALUES ('2', '8', '30', '12', '2', '4', '2', '0', '1439889181', '1439889181');
INSERT INTO `IMMessage_0` VALUES ('3', '8', '30', '12', '3', 'KtPY9224xv/chob0mZhnRw==', '1', '0', '1439889220', '1439889220');
INSERT INTO `IMMessage_0` VALUES ('4', '8', '30', '12', '4', 'chGkFieoeZnvCBVcYX2OuQ==', '1', '0', '1439889224', '1439889224');
INSERT INTO `IMMessage_0` VALUES ('5', '16', '13', '14', '1', 'Aa10jsZ6exeDPY0Sxtp+Pg==', '1', '0', '1439911903', '1439911903');
INSERT INTO `IMMessage_0` VALUES ('6', '16', '13', '14', '2', 'ckNlvnCkRelkFSu+H8Z8yg==', '1', '0', '1439912005', '1439912005');
INSERT INTO `IMMessage_0` VALUES ('7', '24', '35', '28', '1', 'gkELJinpTMWnofK9zgPoNnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439972829', '1439972829');
INSERT INTO `IMMessage_0` VALUES ('8', '24', '28', '35', '2', '8OzPM/g2dRk5eYH0aNCEBQ==', '1', '0', '1439972958', '1439972958');
INSERT INTO `IMMessage_0` VALUES ('9', '24', '28', '35', '3', '11c2By3xL9S58aPr7lglbAr+GZ43+2mMfQMFqTsGBjY=', '1', '0', '1439974045', '1439974045');
INSERT INTO `IMMessage_0` VALUES ('10', '32', '41', '34', '1', 'gkELJinpTMWnofK9zgPoNnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440409399', '1440409399');
INSERT INTO `IMMessage_0` VALUES ('11', '32', '41', '34', '2', '14', '2', '0', '1440409635', '1440409635');
INSERT INTO `IMMessage_0` VALUES ('12', '40', '64', '41', '1', 'gkELJinpTMWnofK9zgPoNnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440689900', '1440689900');
INSERT INTO `IMMessage_0` VALUES ('13', '40', '64', '41', '2', 'XJGVB14N561q7GFM8q1+U3WXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440689901', '1440689901');
INSERT INTO `IMMessage_0` VALUES ('14', '40', '64', '41', '3', '8JPkwJBnwQMitiGrTbeX2HWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440689902', '1440689902');
INSERT INTO `IMMessage_0` VALUES ('15', '40', '64', '41', '4', 'hTCf9/bYTPozUZsKxwg6CnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440689902', '1440689902');
INSERT INTO `IMMessage_0` VALUES ('16', '40', '64', '41', '5', 'dl+OEcHbCL6f0v0q2QNfRA==', '1', '0', '1440691115', '1440691115');
INSERT INTO `IMMessage_0` VALUES ('17', '40', '64', '41', '6', 'zjBlasLGp0//4WUXczYRvw==', '1', '0', '1440691118', '1440691118');

-- ----------------------------
-- Table structure for IMMessage_1
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_1`;
CREATE TABLE `IMMessage_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_1
-- ----------------------------
INSERT INTO `IMMessage_1` VALUES ('1', '1', '8', '2', '1', '9IELmyN7fAqqsTe5bGFUSg==', '1', '0', '1439427011', '1439427011');
INSERT INTO `IMMessage_1` VALUES ('2', '1', '2', '8', '2', 'Yhw4ddzH4UJZzB5Znem2ig==', '1', '0', '1439654023', '1439654023');
INSERT INTO `IMMessage_1` VALUES ('3', '9', '30', '22', '1', 'chGkFieoeZnvCBVcYX2OuQ==', '1', '0', '1439889238', '1439889238');
INSERT INTO `IMMessage_1` VALUES ('4', '17', '13', '9', '1', 'WNlEGhx6t0e2cs25bWAW3A==', '1', '0', '1439911928', '1439911928');
INSERT INTO `IMMessage_1` VALUES ('5', '25', '35', '37', '1', 'Xn3UjnygkscVxvMnrvYwRg==', '1', '0', '1439996446', '1439996446');
INSERT INTO `IMMessage_1` VALUES ('6', '25', '35', '37', '2', 'qhNqCFdnwq2CTnAZHFBZKHWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439996462', '1439996462');
INSERT INTO `IMMessage_1` VALUES ('7', '25', '37', '35', '3', '8OzPM/g2dRk5eYH0aNCEBQ==', '1', '0', '1439996473', '1439996473');
INSERT INTO `IMMessage_1` VALUES ('8', '25', '37', '35', '4', '8OzPM/g2dRk5eYH0aNCEBQ==', '1', '0', '1439996481', '1439996481');
INSERT INTO `IMMessage_1` VALUES ('9', '33', '41', '2', '1', 'MLnF8Avj9pVc9mdA2ijFtRLyLS1oMWHobm8prY23DtU=', '1', '0', '1440409987', '1440409987');
INSERT INTO `IMMessage_1` VALUES ('10', '41', '65', '64', '1', 'MLnF8Avj9pVc9mdA2ijFtRLyLS1oMWHobm8prY23DtU=', '1', '0', '1440691549', '1440691549');
INSERT INTO `IMMessage_1` VALUES ('11', '41', '65', '64', '2', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691575', '1440691575');
INSERT INTO `IMMessage_1` VALUES ('12', '41', '65', '64', '3', 'vjhoCUd8Dvr88PbTOqGn0nWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691603', '1440691603');
INSERT INTO `IMMessage_1` VALUES ('13', '41', '65', '64', '4', 'M1xntyxB8Wu5zgjSswbKgXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691608', '1440691608');
INSERT INTO `IMMessage_1` VALUES ('14', '41', '65', '64', '5', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691614', '1440691614');
INSERT INTO `IMMessage_1` VALUES ('15', '41', '65', '64', '6', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691614', '1440691614');
INSERT INTO `IMMessage_1` VALUES ('16', '41', '65', '64', '7', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691614', '1440691614');
INSERT INTO `IMMessage_1` VALUES ('17', '41', '65', '64', '8', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691615', '1440691615');
INSERT INTO `IMMessage_1` VALUES ('18', '41', '65', '64', '9', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440691615', '1440691615');
INSERT INTO `IMMessage_1` VALUES ('19', '41', '65', '64', '10', 'MLnF8Avj9pVc9mdA2ijFtRLyLS1oMWHobm8prY23DtU=', '1', '0', '1440691653', '1440691653');
INSERT INTO `IMMessage_1` VALUES ('20', '41', '65', '64', '11', 'isFbgVCILeNPXzVe+MncXQ==', '1', '0', '1440691740', '1440691740');
INSERT INTO `IMMessage_1` VALUES ('21', '41', '64', '65', '12', 'b8iDWP8x2TghweC3yneT7Q==', '1', '0', '1440691764', '1440691764');

-- ----------------------------
-- Table structure for IMMessage_2
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_2`;
CREATE TABLE `IMMessage_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_2
-- ----------------------------
INSERT INTO `IMMessage_2` VALUES ('1', '2', '13', '12', '1', 'tiPUUnGwhpJRmekEMoTnMA==', '1', '0', '1439474778', '1439474778');
INSERT INTO `IMMessage_2` VALUES ('2', '2', '13', '12', '2', '1', '2', '0', '1439474789', '1439474789');
INSERT INTO `IMMessage_2` VALUES ('3', '2', '13', '12', '3', '/C4F90KIxNwdAfm0o24fhQ==', '1', '0', '1439474810', '1439474810');
INSERT INTO `IMMessage_2` VALUES ('4', '10', '30', '18', '1', 'UKheN9KmjZEOcj7ZdGedvw==', '1', '0', '1439889250', '1439889250');
INSERT INTO `IMMessage_2` VALUES ('5', '18', '13', '2', '1', '5xt2kYRS0KocnGJAsSSEYg==', '1', '0', '1439914282', '1439914282');
INSERT INTO `IMMessage_2` VALUES ('6', '26', '38', '19', '1', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440127523', '1440127523');
INSERT INTO `IMMessage_2` VALUES ('7', '26', '19', '38', '2', 'dscnnibYAHCkpQenMd87GQ==', '1', '0', '1440326036', '1440326036');
INSERT INTO `IMMessage_2` VALUES ('8', '34', '41', '37', '1', 'qhNqCFdnwq2CTnAZHFBZKHWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440410353', '1440410353');
INSERT INTO `IMMessage_2` VALUES ('9', '34', '41', '37', '2', '15', '2', '0', '1440410374', '1440410374');

-- ----------------------------
-- Table structure for IMMessage_3
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_3`;
CREATE TABLE `IMMessage_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_3
-- ----------------------------
INSERT INTO `IMMessage_3` VALUES ('1', '3', '13', '8', '1', 'r1OmfBiueQ2d3xFUS+1BkA==', '1', '0', '1439475014', '1439475014');
INSERT INTO `IMMessage_3` VALUES ('2', '11', '13', '6', '1', 'Ue9/XTc70ANv9dXClRv/0A==', '1', '0', '1439911375', '1439911375');
INSERT INTO `IMMessage_3` VALUES ('3', '19', '13', '23', '1', 'egdBznyxZnruoziCrG8+nw==', '1', '0', '1439914296', '1439914296');
INSERT INTO `IMMessage_3` VALUES ('4', '27', '20', '9', '1', 'vjhoCUd8Dvr88PbTOqGn0nWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440151112', '1440151112');
INSERT INTO `IMMessage_3` VALUES ('5', '27', '20', '9', '2', 'zL6M+McU0nfiklCiWjAkEHWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440151167', '1440151167');
INSERT INTO `IMMessage_3` VALUES ('6', '27', '20', '9', '3', '0MoseN2wiZ3+h2GlBkAxYhLyLS1oMWHobm8prY23DtU=', '1', '0', '1440151169', '1440151169');
INSERT INTO `IMMessage_3` VALUES ('7', '35', '49', '33', '1', '4NFcvzJLUBI8kSiNa8C6tg==', '1', '0', '1440422606', '1440422606');
INSERT INTO `IMMessage_3` VALUES ('8', '35', '49', '33', '2', '16', '2', '0', '1440422614', '1440422614');

-- ----------------------------
-- Table structure for IMMessage_4
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_4`;
CREATE TABLE `IMMessage_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_4
-- ----------------------------
INSERT INTO `IMMessage_4` VALUES ('1', '4', '13', '11', '1', 'BNJ7eK9BjgHmoU13cv8J3w==', '1', '0', '1439475023', '1439475023');
INSERT INTO `IMMessage_4` VALUES ('2', '12', '13', '16', '1', 'dgO0LJgOMk6EA5JokxF/WA==', '1', '0', '1439911387', '1439911387');
INSERT INTO `IMMessage_4` VALUES ('3', '4', '13', '11', '2', 'gPA1+fV7lS+JMiV/tftrhA==', '1', '0', '1439914312', '1439914312');
INSERT INTO `IMMessage_4` VALUES ('4', '20', '32', '13', '1', 'mJ5TBuWRVx+966f+hk9ebw==', '1', '0', '1439945884', '1439945884');
INSERT INTO `IMMessage_4` VALUES ('5', '28', '39', '33', '1', '7cAig/r0+0qNeyLCWPT9/A==', '1', '0', '1440168041', '1440168041');
INSERT INTO `IMMessage_4` VALUES ('6', '28', '39', '33', '2', 'YgNE9MwVNrQzJvuUTlgfxT+k4Ex4XWM2LkV0mxtGejk=', '1', '0', '1440168045', '1440168045');
INSERT INTO `IMMessage_4` VALUES ('7', '28', '39', '33', '3', 'ib/r0oz2Bwnl2peGD74yag==', '1', '0', '1440168047', '1440168047');
INSERT INTO `IMMessage_4` VALUES ('8', '28', '39', '33', '4', 'ib/r0oz2Bwnl2peGD74yag==', '1', '0', '1440168048', '1440168048');
INSERT INTO `IMMessage_4` VALUES ('9', '28', '39', '33', '5', '0nzp6rWsVjZe3tKg0dvFMA==', '1', '0', '1440168052', '1440168052');
INSERT INTO `IMMessage_4` VALUES ('10', '28', '39', '33', '6', 'ib/r0oz2Bwnl2peGD74yag==', '1', '0', '1440168053', '1440168053');
INSERT INTO `IMMessage_4` VALUES ('11', '28', '39', '33', '7', 'mzxZ5THiRdXr3V8LaacQzQ==', '1', '0', '1440168054', '1440168054');
INSERT INTO `IMMessage_4` VALUES ('12', '28', '39', '33', '8', '8kQLxR3YTuz0ehwAXz55vQ==', '1', '0', '1440168055', '1440168055');
INSERT INTO `IMMessage_4` VALUES ('13', '28', '39', '33', '9', 'jV8EeURmeBvzvox1oL6YSw==', '1', '0', '1440168058', '1440168058');
INSERT INTO `IMMessage_4` VALUES ('14', '28', '39', '33', '10', 'Jak6pPgJt9SMaLgv6CJSZA==', '1', '0', '1440168059', '1440168059');
INSERT INTO `IMMessage_4` VALUES ('15', '28', '39', '33', '11', '6zKZIFnst9ReoJbKuLMBsQ==', '1', '0', '1440168061', '1440168061');
INSERT INTO `IMMessage_4` VALUES ('16', '28', '39', '33', '12', 'vk+mPYUiEeTuDqrLmQpyrFpgkTAdxdMA+POzI430Pu0=', '1', '0', '1440168064', '1440168064');
INSERT INTO `IMMessage_4` VALUES ('17', '28', '39', '33', '13', 'Jak6pPgJt9SMaLgv6CJSZA==', '1', '0', '1440168066', '1440168066');
INSERT INTO `IMMessage_4` VALUES ('18', '28', '39', '33', '14', 'M7txFQdOB662FUQydXXhjw==', '1', '0', '1440168067', '1440168067');
INSERT INTO `IMMessage_4` VALUES ('19', '28', '39', '33', '15', 'yPi1udbJkJvn+R1KCyeZuXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168093', '1440168093');
INSERT INTO `IMMessage_4` VALUES ('20', '28', '39', '33', '16', '9TDjAujvblM0IO5QH8jOVg==', '1', '0', '1440168093', '1440168093');
INSERT INTO `IMMessage_4` VALUES ('21', '28', '39', '33', '17', 'JyC1OKfCqoYoHEtSDas1ZBLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168099', '1440168099');
INSERT INTO `IMMessage_4` VALUES ('22', '28', '39', '33', '18', 'Jak6pPgJt9SMaLgv6CJSZA==', '1', '0', '1440168100', '1440168100');
INSERT INTO `IMMessage_4` VALUES ('23', '28', '39', '33', '19', '0MoseN2wiZ3+h2GlBkAxYhLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168101', '1440168101');
INSERT INTO `IMMessage_4` VALUES ('24', '28', '39', '33', '20', '0MoseN2wiZ3+h2GlBkAxYhLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168101', '1440168101');
INSERT INTO `IMMessage_4` VALUES ('25', '28', '39', '33', '21', 'JyC1OKfCqoYoHEtSDas1ZBLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168101', '1440168101');
INSERT INTO `IMMessage_4` VALUES ('26', '28', '39', '33', '22', '8JPkwJBnwQMitiGrTbeX2HWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168102', '1440168102');
INSERT INTO `IMMessage_4` VALUES ('27', '28', '39', '33', '23', 'Lb4EK6cv+MoSEar8GFYMO3WXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168103', '1440168103');
INSERT INTO `IMMessage_4` VALUES ('28', '28', '39', '33', '24', 'JyC1OKfCqoYoHEtSDas1ZBLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168104', '1440168104');
INSERT INTO `IMMessage_4` VALUES ('29', '28', '39', '33', '25', 'zL6M+McU0nfiklCiWjAkEHWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168104', '1440168104');
INSERT INTO `IMMessage_4` VALUES ('30', '28', '39', '33', '26', '0MoseN2wiZ3+h2GlBkAxYhLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168104', '1440168104');
INSERT INTO `IMMessage_4` VALUES ('31', '28', '39', '33', '27', 'MLnF8Avj9pVc9mdA2ijFtRLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168105', '1440168105');
INSERT INTO `IMMessage_4` VALUES ('32', '28', '39', '33', '28', '8JPkwJBnwQMitiGrTbeX2HWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168105', '1440168105');
INSERT INTO `IMMessage_4` VALUES ('33', '28', '39', '33', '29', 'JyC1OKfCqoYoHEtSDas1ZBLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168105', '1440168105');
INSERT INTO `IMMessage_4` VALUES ('34', '28', '39', '33', '30', 'xHiMKYHeH28J8UhORPMlAnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168105', '1440168105');
INSERT INTO `IMMessage_4` VALUES ('35', '28', '39', '33', '31', 'MLnF8Avj9pVc9mdA2ijFtRLyLS1oMWHobm8prY23DtU=', '1', '0', '1440168105', '1440168105');
INSERT INTO `IMMessage_4` VALUES ('36', '28', '39', '33', '32', 'qhNqCFdnwq2CTnAZHFBZKHWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168107', '1440168107');
INSERT INTO `IMMessage_4` VALUES ('37', '28', '39', '33', '33', 'D4sc2WVwtzz1GyOPKhflLnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168107', '1440168107');
INSERT INTO `IMMessage_4` VALUES ('38', '28', '39', '33', '34', '/vmes0dzLT6PsmfEHPVpf/Ap0qyltkTAYszCyZZcMcjgAjGO8xbyD9tUy63YSSIG', '1', '0', '1440168111', '1440168111');
INSERT INTO `IMMessage_4` VALUES ('39', '28', '39', '33', '35', '6ygkXUf3GUzrDbwoHNhNYQ==', '1', '0', '1440168114', '1440168114');
INSERT INTO `IMMessage_4` VALUES ('40', '28', '39', '33', '36', '6ygkXUf3GUzrDbwoHNhNYQ==', '1', '0', '1440168116', '1440168116');
INSERT INTO `IMMessage_4` VALUES ('41', '28', '39', '33', '37', 'IcI7mZo6qxCcSbBRuQI7nzZZGNbLgUzw4q/9MJRDp0A=', '1', '0', '1440168119', '1440168119');
INSERT INTO `IMMessage_4` VALUES ('42', '28', '39', '33', '38', 'D4sc2WVwtzz1GyOPKhflLnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440168132', '1440168132');
INSERT INTO `IMMessage_4` VALUES ('43', '36', '51', '43', '1', '7FmrAAglFzdONwX8vDX97xV3KsTrQgm4mWPeXHpz1JM=', '1', '0', '1440478718', '1440478718');
INSERT INTO `IMMessage_4` VALUES ('44', '36', '51', '43', '2', 'IDoQFaN+B4z+5Pwx3+s4MA==', '1', '0', '1440478721', '1440478721');

-- ----------------------------
-- Table structure for IMMessage_5
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_5`;
CREATE TABLE `IMMessage_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_5
-- ----------------------------
INSERT INTO `IMMessage_5` VALUES ('1', '5', '17', '12', '1', '2', '2', '0', '1439540628', '1439540628');
INSERT INTO `IMMessage_5` VALUES ('2', '5', '17', '12', '2', 'M1xntyxB8Wu5zgjSswbKgXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439540699', '1439540699');
INSERT INTO `IMMessage_5` VALUES ('3', '5', '17', '12', '3', 'XJGVB14N561q7GFM8q1+U3WXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439540701', '1439540701');
INSERT INTO `IMMessage_5` VALUES ('4', '5', '17', '12', '4', 'txhEto6rCvggSja3GWzPJ1zIdZRUsFN9eOwYZegap80=', '1', '0', '1439540945', '1439540945');
INSERT INTO `IMMessage_5` VALUES ('5', '5', '17', '12', '5', 'dIGazdt1fHx0UuPqq1qPvhxVcRRvlj7PNt3LSNu506FwODrb5J1YtMiMMWLWx4pd3WZyHiE3pxLPj5VtERVpJ3SwsgcqwZmOj1zY7HUGUO+np+aolrEfcA7yd6xMCaDvWkkxinUWYHKRZH+fUU994F1dUHd3Sde2AnSqSke79PxdRarCH5w5undE+AQ0VmMRe1I/P/Tz9yvKpgOC6TO7PZHnjy8EAdgA57e7PRX8YcuZvKf4hxEqzGH/h4f2ImHckKAJWW1DNRmvgPjws3cZMJZtjniFwgvJDf7GlGiZAzY=', '1', '0', '1439540960', '1439540960');
INSERT INTO `IMMessage_5` VALUES ('6', '13', '13', '10', '1', 'J0cjM1XRb6PDSJAI9beODQ==', '1', '0', '1439911396', '1439911396');
INSERT INTO `IMMessage_5` VALUES ('7', '13', '13', '10', '2', '89hhbqc1Jc6wzQWzTlw6fA==', '1', '0', '1439914320', '1439914320');
INSERT INTO `IMMessage_5` VALUES ('8', '21', '32', '31', '1', 'CJO5TKy6mYmJ545nBRqmOQ==', '1', '0', '1439946066', '1439946066');
INSERT INTO `IMMessage_5` VALUES ('9', '21', '32', '31', '2', '5', '2', '0', '1439947276', '1439947276');
INSERT INTO `IMMessage_5` VALUES ('10', '29', '29', '28', '1', '7', '2', '0', '1440346567', '1440346567');
INSERT INTO `IMMessage_5` VALUES ('11', '37', '51', '12', '1', 'aRGMNzJbs7d4Iw1hwU2j5Qr+GZ43+2mMfQMFqTsGBjY=', '1', '0', '1440478766', '1440478766');
INSERT INTO `IMMessage_5` VALUES ('12', '37', '51', '12', '2', 'JyC1OKfCqoYoHEtSDas1ZBLyLS1oMWHobm8prY23DtU=', '1', '0', '1440478772', '1440478772');
INSERT INTO `IMMessage_5` VALUES ('13', '37', '51', '12', '3', 'xHiMKYHeH28J8UhORPMlAnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440478773', '1440478773');
INSERT INTO `IMMessage_5` VALUES ('14', '37', '51', '12', '4', 'gu+LlYsi1l7RioOOL3AN3gr+GZ43+2mMfQMFqTsGBjY=', '1', '0', '1440478780', '1440478780');
INSERT INTO `IMMessage_5` VALUES ('15', '37', '51', '12', '5', '17', '2', '0', '1440478792', '1440478792');
INSERT INTO `IMMessage_5` VALUES ('16', '37', '51', '12', '6', '6H6VoAu09/GpFJiGGpf9XdQ/mY2I/UdPZn3FjyHOboo=', '1', '0', '1440478838', '1440478838');

-- ----------------------------
-- Table structure for IMMessage_6
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_6`;
CREATE TABLE `IMMessage_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_6
-- ----------------------------
INSERT INTO `IMMessage_6` VALUES ('1', '6', '20', '19', '1', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1439542914', '1439542914');
INSERT INTO `IMMessage_6` VALUES ('2', '6', '20', '19', '2', 'qQKEIaRJG4uPy1mpj+Xxyw==', '1', '0', '1439544211', '1439544211');
INSERT INTO `IMMessage_6` VALUES ('3', '6', '19', '20', '3', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1439544277', '1439544277');
INSERT INTO `IMMessage_6` VALUES ('4', '6', '20', '19', '4', '+jor1yfGfbfgJP/WzwIJiA==', '1', '0', '1439544284', '1439544284');
INSERT INTO `IMMessage_6` VALUES ('5', '6', '20', '19', '6', 'gkELJinpTMWnofK9zgPoNnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439544300', '1439544300');
INSERT INTO `IMMessage_6` VALUES ('6', '6', '20', '19', '5', '3', '2', '0', '1439544298', '1439544298');
INSERT INTO `IMMessage_6` VALUES ('7', '6', '19', '20', '7', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1439796097', '1439796097');
INSERT INTO `IMMessage_6` VALUES ('8', '6', '19', '20', '8', '2RHmUQFpvSzS27CrbiFz4VzIdZRUsFN9eOwYZegap80=', '1', '0', '1439796202', '1439796202');
INSERT INTO `IMMessage_6` VALUES ('9', '6', '19', '20', '9', '5TOpW5wt/SwXP/25JV8UDQ==', '1', '0', '1439886332', '1439886332');
INSERT INTO `IMMessage_6` VALUES ('10', '6', '19', '20', '10', '4nFElkhJ6Dh2+qlSEgRUVg==', '1', '0', '1439886350', '1439886350');
INSERT INTO `IMMessage_6` VALUES ('11', '6', '19', '20', '11', 'gAz1U+BBIkOunFRUKGRBElzIdZRUsFN9eOwYZegap80=', '1', '0', '1439886367', '1439886367');
INSERT INTO `IMMessage_6` VALUES ('12', '6', '19', '20', '12', 'hZX4d2enYEo8JWFuObBP5QfU6OB7fpsvZh6GP3dYB9g=', '1', '0', '1439886377', '1439886377');
INSERT INTO `IMMessage_6` VALUES ('13', '6', '19', '20', '13', '95JoXGP3t4Yna51c5JOOUV9+2MgLzpxsxWqI4ngMCkM=', '1', '0', '1439886380', '1439886380');
INSERT INTO `IMMessage_6` VALUES ('14', '6', '20', '19', '14', 'brMcxcmVDxcd1IAs1z4LsXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439886394', '1439886394');
INSERT INTO `IMMessage_6` VALUES ('15', '6', '19', '20', '15', 'arDGTnftzzFkdFUoiVMxcg==', '1', '0', '1439886445', '1439886445');
INSERT INTO `IMMessage_6` VALUES ('16', '14', '13', '3', '1', 'vbUm6ZEAnmWxULf3BE32PA==', '1', '0', '1439911404', '1439911404');
INSERT INTO `IMMessage_6` VALUES ('17', '14', '13', '3', '2', '7rDepPxHWagpGOG1UipcPQ==', '1', '0', '1439914304', '1439914304');
INSERT INTO `IMMessage_6` VALUES ('18', '22', '35', '27', '1', 'jQ6+Gi0KCXxQnXtO5OuSTA==', '1', '0', '1439971415', '1439971415');
INSERT INTO `IMMessage_6` VALUES ('19', '22', '27', '35', '2', 'cHxrCUFg6rysvgX+SxRNaVzIdZRUsFN9eOwYZegap80=', '1', '0', '1439971439', '1439971439');
INSERT INTO `IMMessage_6` VALUES ('20', '22', '35', '27', '3', 't8cns/6sLwRs708FH17pYg==', '1', '0', '1439971455', '1439971455');
INSERT INTO `IMMessage_6` VALUES ('21', '22', '27', '35', '4', 'B2U4n8IThoOPPETw5VPPxdF/nsQAwe6rIlcwFLLLy1g=', '1', '0', '1439971458', '1439971458');
INSERT INTO `IMMessage_6` VALUES ('22', '22', '27', '35', '5', 'M/92Ht5OyoP6GGhjzut+SVzIdZRUsFN9eOwYZegap80=', '1', '0', '1439971464', '1439971464');
INSERT INTO `IMMessage_6` VALUES ('23', '22', '35', '27', '6', '1SONeFXWss2w6E+DsHG+lw==', '1', '0', '1439971471', '1439971471');
INSERT INTO `IMMessage_6` VALUES ('24', '22', '27', '35', '7', '8OzPM/g2dRk5eYH0aNCEBQ==', '1', '0', '1439971486', '1439971486');
INSERT INTO `IMMessage_6` VALUES ('25', '22', '35', '27', '8', '/uAGaW/vB3VcLVal+h8wyUO757U7OmQMZjj5rGV91uz1EV+nWkCfDab/sNuUCZRf', '1', '0', '1439971533', '1439971533');
INSERT INTO `IMMessage_6` VALUES ('26', '22', '27', '35', '9', 'GMthcpg1hoTsY3NG3JnxhTiZ7k9VM7Nskiuu09Z6rXc=', '1', '0', '1439971564', '1439971564');
INSERT INTO `IMMessage_6` VALUES ('27', '22', '35', '27', '10', 'RVwZ5z072X1yuA+i5iAXAA==', '1', '0', '1439971625', '1439971625');
INSERT INTO `IMMessage_6` VALUES ('28', '22', '27', '35', '11', 'SJZUlf3gF6GjZzT14Pe4sOCeDaAODMn237hE4fTHtqX1EV+nWkCfDab/sNuUCZRf', '1', '0', '1439971708', '1439971708');
INSERT INTO `IMMessage_6` VALUES ('29', '22', '35', '27', '12', 'Nuvr3w7mg/uSpkBYsz46/w==', '1', '0', '1439971830', '1439971830');
INSERT INTO `IMMessage_6` VALUES ('30', '22', '35', '27', '13', 'IFOLFPYtkiDh9RWsHCaYVwo1mmMyVn5zZIvS0svlP2HpHsUUBA8PusqXtciFBc57Zy09PHRz2BuCJJ1CpYpEsg==', '1', '0', '1439972286', '1439972286');
INSERT INTO `IMMessage_6` VALUES ('31', '6', '19', '20', '16', '/niSnV4V/c/ObQHGNWfRnQ==', '1', '0', '1440150983', '1440150983');
INSERT INTO `IMMessage_6` VALUES ('32', '6', '20', '19', '17', 'iP37lPjLnSQBytMVNXZlQw==', '1', '0', '1440150994', '1440150994');
INSERT INTO `IMMessage_6` VALUES ('33', '6', '20', '19', '18', 'YMhfonWIcq7TLAv+wPWa6A==', '1', '0', '1440150998', '1440150998');
INSERT INTO `IMMessage_6` VALUES ('34', '6', '19', '20', '19', 'WbRuAk8XAFoF4pQ6BVNDOw==', '1', '0', '1440151006', '1440151006');
INSERT INTO `IMMessage_6` VALUES ('35', '6', '20', '19', '20', '4jj+IB/5TWBqT+mIhlJvag==', '1', '0', '1440151012', '1440151012');
INSERT INTO `IMMessage_6` VALUES ('36', '6', '19', '20', '21', 'gmWZcdEMLqeFhza/EiSxDQ==', '1', '0', '1440151020', '1440151020');
INSERT INTO `IMMessage_6` VALUES ('37', '6', '20', '19', '22', 'rcc41ldo4Mj9/Xc+9uJynA==', '1', '0', '1440151032', '1440151032');
INSERT INTO `IMMessage_6` VALUES ('38', '6', '20', '19', '23', 'D4sc2WVwtzz1GyOPKhflLnWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1440151085', '1440151085');
INSERT INTO `IMMessage_6` VALUES ('39', '6', '19', '20', '24', 'TAJcPsfVH5gO1LtbrWYjHA==', '1', '0', '1440326274', '1440326274');
INSERT INTO `IMMessage_6` VALUES ('40', '6', '20', '19', '25', 'nGGqig/crsqcH9x0euXjzA==', '1', '0', '1440326306', '1440326306');
INSERT INTO `IMMessage_6` VALUES ('41', '6', '20', '19', '26', 'UKheN9KmjZEOcj7ZdGedvw==', '1', '0', '1440337119', '1440337119');
INSERT INTO `IMMessage_6` VALUES ('42', '6', '19', '20', '27', 'VkMfabCuK6QJEWSsocqFpw==', '1', '0', '1440340580', '1440340580');
INSERT INTO `IMMessage_6` VALUES ('43', '6', '19', '20', '28', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1440344915', '1440344915');
INSERT INTO `IMMessage_6` VALUES ('44', '6', '19', '20', '29', '+jor1yfGfbfgJP/WzwIJiA==', '1', '0', '1440344920', '1440344920');
INSERT INTO `IMMessage_6` VALUES ('45', '6', '19', '20', '30', '8', '2', '0', '1440393297', '1440393297');
INSERT INTO `IMMessage_6` VALUES ('46', '6', '19', '20', '31', '9', '2', '0', '1440393320', '1440393320');
INSERT INTO `IMMessage_6` VALUES ('47', '6', '19', '20', '32', '10', '2', '0', '1440393326', '1440393326');
INSERT INTO `IMMessage_6` VALUES ('48', '6', '20', '19', '33', '11', '2', '0', '1440393345', '1440393345');
INSERT INTO `IMMessage_6` VALUES ('49', '6', '19', '20', '34', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1440400961', '1440400961');
INSERT INTO `IMMessage_6` VALUES ('50', '6', '19', '20', '35', '+jor1yfGfbfgJP/WzwIJiA==', '1', '0', '1440400972', '1440400972');
INSERT INTO `IMMessage_6` VALUES ('51', '6', '19', '20', '36', 'kZzcyyewYZ9bqPUyEZa20g==', '1', '0', '1440401048', '1440401048');
INSERT INTO `IMMessage_6` VALUES ('52', '6', '20', '19', '37', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1440401200', '1440401200');
INSERT INTO `IMMessage_6` VALUES ('53', '6', '20', '19', '38', '+jor1yfGfbfgJP/WzwIJiA==', '1', '0', '1440401202', '1440401202');
INSERT INTO `IMMessage_6` VALUES ('54', '6', '20', '19', '39', 'kZzcyyewYZ9bqPUyEZa20g==', '1', '0', '1440401204', '1440401204');
INSERT INTO `IMMessage_6` VALUES ('55', '6', '20', '19', '40', 'dscnnibYAHCkpQenMd87GQ==', '1', '0', '1440402156', '1440402156');
INSERT INTO `IMMessage_6` VALUES ('56', '30', '40', '20', '1', 'dscnnibYAHCkpQenMd87GQ==', '1', '0', '1440402230', '1440402230');
INSERT INTO `IMMessage_6` VALUES ('57', '30', '20', '40', '2', 'dscnnibYAHCkpQenMd87GQ==', '1', '0', '1440402236', '1440402236');
INSERT INTO `IMMessage_6` VALUES ('58', '30', '40', '20', '3', 'EfVyFUzKp1vxlozGFIWp+1zIdZRUsFN9eOwYZegap80=', '1', '0', '1440402429', '1440402429');
INSERT INTO `IMMessage_6` VALUES ('59', '38', '56', '3', '1', '18', '2', '0', '1440491662', '1440491662');

-- ----------------------------
-- Table structure for IMMessage_7
-- ----------------------------
DROP TABLE IF EXISTS `IMMessage_7`;
CREATE TABLE `IMMessage_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `relateId` int(11) unsigned NOT NULL COMMENT '用户的关系id',
  `fromId` int(11) unsigned NOT NULL COMMENT '发送用户的id',
  `toId` int(11) unsigned NOT NULL COMMENT '接收用户的id',
  `msgId` int(11) unsigned NOT NULL COMMENT '消息ID',
  `content` varchar(4096) COLLATE utf8mb4_bin DEFAULT '' COMMENT '消息内容',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '消息类型',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常 1被删除',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_relateId_status_created` (`relateId`,`status`,`created`),
  KEY `idx_relateId_status_msgId_created` (`relateId`,`status`,`msgId`,`created`),
  KEY `idx_fromId_toId_created` (`fromId`,`toId`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMMessage_7
-- ----------------------------
INSERT INTO `IMMessage_7` VALUES ('1', '7', '19', '13', '1', 'B+HuUVixIt9S2wq8XgAH9A==', '1', '0', '1439542971', '1439542971');
INSERT INTO `IMMessage_7` VALUES ('2', '7', '13', '19', '2', 'H8oShvLrRs1LkoCbRam/Tw==', '1', '0', '1439910953', '1439910953');
INSERT INTO `IMMessage_7` VALUES ('3', '15', '13', '15', '1', 'EXkmMrrptgZtPrBn1ghPCg==', '1', '0', '1439911818', '1439911818');
INSERT INTO `IMMessage_7` VALUES ('4', '23', '35', '34', '1', 'yPi1udbJkJvn+R1KCyeZuXWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439972363', '1439972363');
INSERT INTO `IMMessage_7` VALUES ('5', '23', '35', '34', '2', 'MLnF8Avj9pVc9mdA2ijFtRLyLS1oMWHobm8prY23DtU=', '1', '0', '1439972434', '1439972434');
INSERT INTO `IMMessage_7` VALUES ('6', '23', '35', '34', '3', 'zL6M+McU0nfiklCiWjAkEHWXrZFfV+UBo37vcxL/NUY=', '1', '0', '1439972437', '1439972437');
INSERT INTO `IMMessage_7` VALUES ('7', '23', '35', '34', '4', 'p85aMEh5ZITwSmPzANMmlQ==', '1', '0', '1439972460', '1439972460');
INSERT INTO `IMMessage_7` VALUES ('8', '23', '35', '34', '5', 'tyWjqF9371fMfb6pEy38lw==', '1', '0', '1439972604', '1439972604');
INSERT INTO `IMMessage_7` VALUES ('9', '23', '35', '34', '6', 'DCVUToS2A/zTcnBfLZtV0w==', '1', '0', '1439973088', '1439973088');
INSERT INTO `IMMessage_7` VALUES ('10', '23', '35', '34', '7', 'yp3pVySuHw9xqN1ncOAcwA==', '1', '0', '1439996357', '1439996357');
INSERT INTO `IMMessage_7` VALUES ('11', '23', '35', '34', '8', 'Xn3UjnygkscVxvMnrvYwRg==', '1', '0', '1439996382', '1439996382');
INSERT INTO `IMMessage_7` VALUES ('12', '31', '41', '28', '1', 'trvgs+4qC/og8lRK9T73lg==', '1', '0', '1440409134', '1440409134');
INSERT INTO `IMMessage_7` VALUES ('13', '39', '59', '28', '1', '19', '2', '0', '1440577613', '1440577613');

-- ----------------------------
-- Table structure for IMNode
-- ----------------------------
DROP TABLE IF EXISTS `IMNode`;
CREATE TABLE `IMNode` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL,
  `title` char(50) DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `remark` char(255) DEFAULT NULL,
  `sort` int(6) unsigned DEFAULT NULL,
  `pid` int(6) unsigned NOT NULL,
  `level` int(1) unsigned NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `group_id` int(3) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `pid` (`pid`),
  KEY `status` (`status`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=229 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMNode
-- ----------------------------
INSERT INTO `IMNode` VALUES ('50', 'main', '空白首页', '1', '', null, '40', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('84', 'GroupAdmin', '分组管理', '1', '', '1', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('90', 'Article', '文章管理', '0', '', '100', '1', '2', '0', '9');
INSERT INTO `IMNode` VALUES ('93', 'index', '首页管理', '1', '', '0', '92', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('96', 'index', '列表页', '1', '', '0', '95', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('1', 'Admin', '后台管理', '1', '', '0', '0', '1', '0', '0');
INSERT INTO `IMNode` VALUES ('2', 'Node', '节点管理', '1', '', '2', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('6', 'Role', '角色管理', '1', '', '1', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('7', 'AdminUser', '后台用户', '1', '', '3', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('30', 'Public', '公共模块', '1', '', '5', '1', '2', '0', '0');
INSERT INTO `IMNode` VALUES ('31', 'add', '新增', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('32', 'insert', '写入', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('33', 'edit', '编辑', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('39', 'index', '列表', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('37', 'resume', '恢复', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('36', 'forbid', '禁用', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('34', 'update', '更新', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('35', 'foreverdelete', '删除', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('40', 'Index', '默认模块', '1', '', '1', '1', '2', '0', '0');
INSERT INTO `IMNode` VALUES ('49', 'read', '查看', '1', '', null, '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('125', 'index', '首页模块', '1', null, null, '40', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('127', 'Log', '登录日志管理', '1', '', '8', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('133', 'index', '首页', '1', '', '0', '132', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('152', 'detail', '详情', '1', '', '0', '30', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('161', 'Bak', '数据库备份', '1', '', '6', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('163', 'GroupClass', '系统导航管理', '1', '', '0', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('168', 'File', '程序文件管理', '1', '', '10', '1', '2', '0', '29');
INSERT INTO `IMNode` VALUES ('204', 'index', '列表', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('208', 'index', '列表', '1', '', '100', '168', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('209', 'add', '新增', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('210', 'edit', '修改', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('211', 'index', '列表', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('207', 'foreverdelete', '删除', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('206', 'edit', '修改', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('205', 'add', '新增', '1', '', '100', '90', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('212', 'foreverdelete', '删除', '1', '', '100', '84', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('213', 'foreverdelete', '删除', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('203', 'add', '添加', '1', '', '100', '202', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('214', 'add', '新增', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('215', 'edit', '修改', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('216', 'index', '列表', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('217', 'password', '修改密码', '1', '', '100', '7', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('219', 'foreverdelete', '删除', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('220', 'add', '新增', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('221', 'edit', '修改', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('222', 'index', '列表', '1', '', '100', '2', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('223', 'Notice', '全局通知配置', '1', '', '100', '1', '2', '0', '2');
INSERT INTO `IMNode` VALUES ('224', 'Users', '统一用户数据', '1', '', '100', '1', '2', '0', '9');
INSERT INTO `IMNode` VALUES ('225', 'index', '首页', '1', '', '100', '224', '3', '0', '0');
INSERT INTO `IMNode` VALUES ('226', 'Discovery', '发现URL管理', '1', '', '100', '1', '2', '0', '9');
INSERT INTO `IMNode` VALUES ('227', 'Group', '用户群管理', '1', '', '100', '1', '2', '0', '9');
INSERT INTO `IMNode` VALUES ('228', 'Depart', '组织部门管理', '1', '', '100', '1', '2', '0', '9');

-- ----------------------------
-- Table structure for IMNotice
-- ----------------------------
DROP TABLE IF EXISTS `IMNotice`;
CREATE TABLE `IMNotice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `times` varchar(255) DEFAULT NULL,
  `agentid` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(10) unsigned zerofill DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMNotice
-- ----------------------------
INSERT INTO `IMNotice` VALUES ('6', '这里是第一条全局测试推送通知标题', '这里是第一条全局测试推送通知内容', '1437410982', '999999', null, '0000000001', '0', null);

-- ----------------------------
-- Table structure for IMRecentSession
-- ----------------------------
DROP TABLE IF EXISTS `IMRecentSession`;
CREATE TABLE `IMRecentSession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) unsigned NOT NULL COMMENT '用户id',
  `peerId` int(11) unsigned NOT NULL COMMENT '对方id',
  `type` tinyint(1) unsigned DEFAULT '0' COMMENT '类型，1-用户,2-群组',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '用户:0-正常, 1-用户A删除,群组:0-正常, 1-被删除',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_userId_peerId_status_updated` (`userId`,`peerId`,`status`,`updated`),
  KEY `idx_userId_peerId_type` (`userId`,`peerId`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRecentSession
-- ----------------------------
INSERT INTO `IMRecentSession` VALUES ('1', '6', '1', '2', '0', '1439390139', '1439390514');
INSERT INTO `IMRecentSession` VALUES ('2', '2', '1', '2', '0', '1439390140', '1439390514');
INSERT INTO `IMRecentSession` VALUES ('3', '3', '1', '2', '0', '1439390140', '1439390514');
INSERT INTO `IMRecentSession` VALUES ('4', '8', '2', '1', '0', '1439427011', '1439654023');
INSERT INTO `IMRecentSession` VALUES ('5', '2', '8', '1', '0', '1439427011', '1439654023');
INSERT INTO `IMRecentSession` VALUES ('6', '13', '12', '1', '0', '1439474778', '1439474810');
INSERT INTO `IMRecentSession` VALUES ('7', '12', '13', '1', '0', '1439474778', '1439474810');
INSERT INTO `IMRecentSession` VALUES ('8', '13', '8', '1', '0', '1439475014', '1439475014');
INSERT INTO `IMRecentSession` VALUES ('9', '8', '13', '1', '0', '1439475014', '1439475014');
INSERT INTO `IMRecentSession` VALUES ('10', '13', '11', '1', '0', '1439475023', '1439914312');
INSERT INTO `IMRecentSession` VALUES ('11', '11', '13', '1', '0', '1439475023', '1439914312');
INSERT INTO `IMRecentSession` VALUES ('12', '17', '12', '1', '0', '1439540628', '1439540960');
INSERT INTO `IMRecentSession` VALUES ('13', '12', '17', '1', '0', '1439540628', '1439540960');
INSERT INTO `IMRecentSession` VALUES ('14', '20', '19', '1', '0', '1439542914', '1440402156');
INSERT INTO `IMRecentSession` VALUES ('15', '19', '20', '1', '0', '1439542914', '1440402156');
INSERT INTO `IMRecentSession` VALUES ('16', '19', '13', '1', '1', '1439542971', '1440339128');
INSERT INTO `IMRecentSession` VALUES ('17', '13', '19', '1', '0', '1439542971', '1439910953');
INSERT INTO `IMRecentSession` VALUES ('18', '30', '12', '1', '0', '1439889147', '1439889224');
INSERT INTO `IMRecentSession` VALUES ('19', '12', '30', '1', '0', '1439889147', '1439889224');
INSERT INTO `IMRecentSession` VALUES ('20', '30', '22', '1', '0', '1439889238', '1439889238');
INSERT INTO `IMRecentSession` VALUES ('21', '22', '30', '1', '0', '1439889238', '1439889238');
INSERT INTO `IMRecentSession` VALUES ('22', '30', '18', '1', '0', '1439889250', '1439889250');
INSERT INTO `IMRecentSession` VALUES ('23', '18', '30', '1', '0', '1439889250', '1439889250');
INSERT INTO `IMRecentSession` VALUES ('24', '30', '2', '2', '0', '1439889518', '1439889518');
INSERT INTO `IMRecentSession` VALUES ('25', '12', '2', '2', '0', '1439889521', '1439889521');
INSERT INTO `IMRecentSession` VALUES ('26', '18', '2', '2', '0', '1439889521', '1439889521');
INSERT INTO `IMRecentSession` VALUES ('27', '24', '2', '2', '0', '1439889521', '1439889521');
INSERT INTO `IMRecentSession` VALUES ('28', '13', '3', '2', '0', '1439910924', '1439910927');
INSERT INTO `IMRecentSession` VALUES ('29', '12', '3', '2', '0', '1439910928', '1439910927');
INSERT INTO `IMRecentSession` VALUES ('30', '6', '3', '2', '0', '1439910928', '1439910927');
INSERT INTO `IMRecentSession` VALUES ('31', '13', '6', '1', '0', '1439911375', '1439911375');
INSERT INTO `IMRecentSession` VALUES ('32', '6', '13', '1', '0', '1439911375', '1439911375');
INSERT INTO `IMRecentSession` VALUES ('33', '13', '16', '1', '0', '1439911387', '1439911387');
INSERT INTO `IMRecentSession` VALUES ('34', '16', '13', '1', '0', '1439911387', '1439911387');
INSERT INTO `IMRecentSession` VALUES ('35', '13', '10', '1', '0', '1439911396', '1439914320');
INSERT INTO `IMRecentSession` VALUES ('36', '10', '13', '1', '0', '1439911396', '1439914320');
INSERT INTO `IMRecentSession` VALUES ('37', '13', '3', '1', '0', '1439911404', '1439914304');
INSERT INTO `IMRecentSession` VALUES ('38', '3', '13', '1', '0', '1439911404', '1439914304');
INSERT INTO `IMRecentSession` VALUES ('39', '13', '4', '2', '0', '1439911444', '1439911444');
INSERT INTO `IMRecentSession` VALUES ('40', '3', '4', '2', '0', '1439911449', '1439911449');
INSERT INTO `IMRecentSession` VALUES ('41', '6', '4', '2', '0', '1439911449', '1439911449');
INSERT INTO `IMRecentSession` VALUES ('42', '13', '15', '1', '0', '1439911818', '1439911818');
INSERT INTO `IMRecentSession` VALUES ('43', '15', '13', '1', '0', '1439911818', '1439911818');
INSERT INTO `IMRecentSession` VALUES ('44', '13', '14', '1', '0', '1439911903', '1439912005');
INSERT INTO `IMRecentSession` VALUES ('45', '14', '13', '1', '0', '1439911903', '1439912005');
INSERT INTO `IMRecentSession` VALUES ('46', '13', '9', '1', '0', '1439911928', '1439911928');
INSERT INTO `IMRecentSession` VALUES ('47', '9', '13', '1', '0', '1439911928', '1439911928');
INSERT INTO `IMRecentSession` VALUES ('48', '13', '6', '2', '0', '1439911987', '1439912015');
INSERT INTO `IMRecentSession` VALUES ('49', '15', '6', '2', '0', '1439911988', '1439912015');
INSERT INTO `IMRecentSession` VALUES ('50', '9', '6', '2', '0', '1439911988', '1439912015');
INSERT INTO `IMRecentSession` VALUES ('51', '13', '5', '2', '0', '1439911993', '1439911993');
INSERT INTO `IMRecentSession` VALUES ('52', '14', '5', '2', '0', '1439911998', '1439911998');
INSERT INTO `IMRecentSession` VALUES ('53', '9', '5', '2', '0', '1439911998', '1439911998');
INSERT INTO `IMRecentSession` VALUES ('54', '13', '2', '1', '0', '1439914282', '1439914282');
INSERT INTO `IMRecentSession` VALUES ('55', '2', '13', '1', '0', '1439914282', '1439914282');
INSERT INTO `IMRecentSession` VALUES ('56', '13', '23', '1', '0', '1439914296', '1439914296');
INSERT INTO `IMRecentSession` VALUES ('57', '23', '13', '1', '0', '1439914296', '1439914296');
INSERT INTO `IMRecentSession` VALUES ('58', '32', '13', '1', '0', '1439945884', '1439945884');
INSERT INTO `IMRecentSession` VALUES ('59', '13', '32', '1', '0', '1439945884', '1439945884');
INSERT INTO `IMRecentSession` VALUES ('60', '32', '31', '1', '0', '1439946066', '1439947276');
INSERT INTO `IMRecentSession` VALUES ('61', '31', '32', '1', '0', '1439946066', '1439947276');
INSERT INTO `IMRecentSession` VALUES ('62', '32', '7', '2', '0', '1439947872', '1439947883');
INSERT INTO `IMRecentSession` VALUES ('63', '21', '7', '2', '0', '1439947873', '1439947883');
INSERT INTO `IMRecentSession` VALUES ('64', '22', '7', '2', '0', '1439947873', '1439947883');
INSERT INTO `IMRecentSession` VALUES ('65', '9', '7', '2', '0', '1439947873', '1439947883');
INSERT INTO `IMRecentSession` VALUES ('66', '35', '27', '1', '0', '1439971415', '1439972286');
INSERT INTO `IMRecentSession` VALUES ('67', '27', '35', '1', '0', '1439971415', '1439972286');
INSERT INTO `IMRecentSession` VALUES ('68', '27', '8', '2', '0', '1439972288', '1439972342');
INSERT INTO `IMRecentSession` VALUES ('69', '35', '8', '2', '0', '1439972289', '1439972342');
INSERT INTO `IMRecentSession` VALUES ('70', '35', '34', '1', '0', '1439972363', '1439996382');
INSERT INTO `IMRecentSession` VALUES ('71', '34', '35', '1', '0', '1439972363', '1439996382');
INSERT INTO `IMRecentSession` VALUES ('72', '35', '28', '1', '0', '1439972829', '1439974045');
INSERT INTO `IMRecentSession` VALUES ('73', '28', '35', '1', '0', '1439972829', '1439974045');
INSERT INTO `IMRecentSession` VALUES ('74', '28', '9', '2', '0', '1439973430', '1439973576');
INSERT INTO `IMRecentSession` VALUES ('75', '35', '9', '2', '0', '1439973435', '1439973576');
INSERT INTO `IMRecentSession` VALUES ('76', '37', '10', '2', '0', '1439973717', '1439978554');
INSERT INTO `IMRecentSession` VALUES ('77', '27', '10', '2', '0', '1439973719', '1439978554');
INSERT INTO `IMRecentSession` VALUES ('78', '34', '10', '2', '0', '1439973719', '1439978554');
INSERT INTO `IMRecentSession` VALUES ('79', '35', '10', '2', '0', '1439973719', '1439978554');
INSERT INTO `IMRecentSession` VALUES ('80', '28', '10', '2', '0', '1439973758', '1439978554');
INSERT INTO `IMRecentSession` VALUES ('81', '30', '10', '2', '0', '1439973828', '1439978554');
INSERT INTO `IMRecentSession` VALUES ('82', '35', '37', '1', '0', '1439996446', '1439996481');
INSERT INTO `IMRecentSession` VALUES ('83', '37', '35', '1', '0', '1439996446', '1439996481');
INSERT INTO `IMRecentSession` VALUES ('84', '38', '19', '1', '0', '1440127523', '1440326036');
INSERT INTO `IMRecentSession` VALUES ('85', '19', '38', '1', '1', '1440127523', '1440339135');
INSERT INTO `IMRecentSession` VALUES ('86', '20', '9', '1', '0', '1440151112', '1440151169');
INSERT INTO `IMRecentSession` VALUES ('87', '9', '20', '1', '0', '1440151112', '1440151169');
INSERT INTO `IMRecentSession` VALUES ('88', '39', '33', '1', '0', '1440168041', '1440168132');
INSERT INTO `IMRecentSession` VALUES ('89', '33', '39', '1', '0', '1440168041', '1440168132');
INSERT INTO `IMRecentSession` VALUES ('90', '29', '28', '1', '0', '1440346567', '1440346567');
INSERT INTO `IMRecentSession` VALUES ('91', '28', '29', '1', '0', '1440346567', '1440346567');
INSERT INTO `IMRecentSession` VALUES ('92', '40', '20', '1', '0', '1440402230', '1440402429');
INSERT INTO `IMRecentSession` VALUES ('93', '20', '40', '1', '0', '1440402230', '1440402429');
INSERT INTO `IMRecentSession` VALUES ('94', '40', '11', '2', '0', '1440402348', '1440404957');
INSERT INTO `IMRecentSession` VALUES ('95', '19', '11', '2', '0', '1440402352', '1440404957');
INSERT INTO `IMRecentSession` VALUES ('96', '20', '11', '2', '0', '1440402352', '1440404957');
INSERT INTO `IMRecentSession` VALUES ('97', '41', '28', '1', '0', '1440409134', '1440409134');
INSERT INTO `IMRecentSession` VALUES ('98', '28', '41', '1', '0', '1440409134', '1440409134');
INSERT INTO `IMRecentSession` VALUES ('99', '41', '34', '1', '0', '1440409399', '1440409635');
INSERT INTO `IMRecentSession` VALUES ('100', '34', '41', '1', '0', '1440409399', '1440409635');
INSERT INTO `IMRecentSession` VALUES ('101', '41', '2', '1', '0', '1440409987', '1440409987');
INSERT INTO `IMRecentSession` VALUES ('102', '2', '41', '1', '0', '1440409987', '1440409987');
INSERT INTO `IMRecentSession` VALUES ('103', '41', '37', '1', '0', '1440410353', '1440410374');
INSERT INTO `IMRecentSession` VALUES ('104', '37', '41', '1', '0', '1440410353', '1440410374');
INSERT INTO `IMRecentSession` VALUES ('105', '49', '33', '1', '0', '1440422606', '1440422614');
INSERT INTO `IMRecentSession` VALUES ('106', '33', '49', '1', '0', '1440422606', '1440422614');
INSERT INTO `IMRecentSession` VALUES ('107', '51', '43', '1', '0', '1440478718', '1440478721');
INSERT INTO `IMRecentSession` VALUES ('108', '43', '51', '1', '0', '1440478718', '1440478721');
INSERT INTO `IMRecentSession` VALUES ('109', '51', '12', '1', '0', '1440478766', '1440478838');
INSERT INTO `IMRecentSession` VALUES ('110', '12', '51', '1', '0', '1440478766', '1440478838');
INSERT INTO `IMRecentSession` VALUES ('111', '51', '12', '2', '0', '1440478876', '1440478876');
INSERT INTO `IMRecentSession` VALUES ('112', '18', '12', '2', '0', '1440478881', '1440478881');
INSERT INTO `IMRecentSession` VALUES ('113', '43', '12', '2', '0', '1440478881', '1440478881');
INSERT INTO `IMRecentSession` VALUES ('114', '46', '12', '2', '0', '1440478881', '1440478881');
INSERT INTO `IMRecentSession` VALUES ('115', '47', '12', '2', '0', '1440478881', '1440478881');
INSERT INTO `IMRecentSession` VALUES ('116', '50', '12', '2', '0', '1440478881', '1440478881');
INSERT INTO `IMRecentSession` VALUES ('117', '56', '3', '1', '0', '1440491662', '1440491662');
INSERT INTO `IMRecentSession` VALUES ('118', '3', '56', '1', '0', '1440491662', '1440491662');
INSERT INTO `IMRecentSession` VALUES ('119', '59', '28', '1', '0', '1440577613', '1440577613');
INSERT INTO `IMRecentSession` VALUES ('120', '28', '59', '1', '0', '1440577613', '1440577613');
INSERT INTO `IMRecentSession` VALUES ('121', '64', '41', '1', '0', '1440689900', '1440691118');
INSERT INTO `IMRecentSession` VALUES ('122', '41', '64', '1', '0', '1440689900', '1440691118');
INSERT INTO `IMRecentSession` VALUES ('123', '65', '64', '1', '0', '1440691549', '1440691764');
INSERT INTO `IMRecentSession` VALUES ('124', '64', '65', '1', '0', '1440691549', '1440691764');

-- ----------------------------
-- Table structure for IMRelationShip
-- ----------------------------
DROP TABLE IF EXISTS `IMRelationShip`;
CREATE TABLE `IMRelationShip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `smallId` int(11) unsigned NOT NULL COMMENT '用户A的id',
  `bigId` int(11) unsigned NOT NULL COMMENT '用户B的id',
  `status` tinyint(1) unsigned DEFAULT '0' COMMENT '用户:0-正常, 1-用户A删除,群组:0-正常, 1-被删除',
  `created` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_smallId_bigId_status_updated` (`smallId`,`bigId`,`status`,`updated`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRelationShip
-- ----------------------------
INSERT INTO `IMRelationShip` VALUES ('1', '2', '8', '0', '1439427011', '1439427011');
INSERT INTO `IMRelationShip` VALUES ('2', '12', '13', '0', '1439474778', '1439474778');
INSERT INTO `IMRelationShip` VALUES ('3', '8', '13', '0', '1439475014', '1439475014');
INSERT INTO `IMRelationShip` VALUES ('4', '11', '13', '0', '1439475023', '1439475023');
INSERT INTO `IMRelationShip` VALUES ('5', '12', '17', '0', '1439540628', '1439540628');
INSERT INTO `IMRelationShip` VALUES ('6', '19', '20', '0', '1439542914', '1439542914');
INSERT INTO `IMRelationShip` VALUES ('7', '13', '19', '0', '1439542971', '1439542971');
INSERT INTO `IMRelationShip` VALUES ('8', '12', '30', '0', '1439889147', '1439889147');
INSERT INTO `IMRelationShip` VALUES ('9', '22', '30', '0', '1439889238', '1439889238');
INSERT INTO `IMRelationShip` VALUES ('10', '18', '30', '0', '1439889250', '1439889250');
INSERT INTO `IMRelationShip` VALUES ('11', '6', '13', '0', '1439911375', '1439911375');
INSERT INTO `IMRelationShip` VALUES ('12', '13', '16', '0', '1439911387', '1439911387');
INSERT INTO `IMRelationShip` VALUES ('13', '10', '13', '0', '1439911396', '1439911396');
INSERT INTO `IMRelationShip` VALUES ('14', '3', '13', '0', '1439911404', '1439911404');
INSERT INTO `IMRelationShip` VALUES ('15', '13', '15', '0', '1439911818', '1439911818');
INSERT INTO `IMRelationShip` VALUES ('16', '13', '14', '0', '1439911903', '1439911903');
INSERT INTO `IMRelationShip` VALUES ('17', '9', '13', '0', '1439911928', '1439911928');
INSERT INTO `IMRelationShip` VALUES ('18', '2', '13', '0', '1439914282', '1439914282');
INSERT INTO `IMRelationShip` VALUES ('19', '13', '23', '0', '1439914296', '1439914296');
INSERT INTO `IMRelationShip` VALUES ('20', '13', '32', '0', '1439945884', '1439945884');
INSERT INTO `IMRelationShip` VALUES ('21', '31', '32', '0', '1439946066', '1439946066');
INSERT INTO `IMRelationShip` VALUES ('22', '27', '35', '0', '1439971415', '1439971415');
INSERT INTO `IMRelationShip` VALUES ('23', '34', '35', '0', '1439972363', '1439972363');
INSERT INTO `IMRelationShip` VALUES ('24', '28', '35', '0', '1439972829', '1439972829');
INSERT INTO `IMRelationShip` VALUES ('25', '35', '37', '0', '1439996446', '1439996446');
INSERT INTO `IMRelationShip` VALUES ('26', '19', '38', '0', '1440127523', '1440127523');
INSERT INTO `IMRelationShip` VALUES ('27', '9', '20', '0', '1440151112', '1440151112');
INSERT INTO `IMRelationShip` VALUES ('28', '33', '39', '0', '1440168041', '1440168041');
INSERT INTO `IMRelationShip` VALUES ('29', '28', '29', '0', '1440346567', '1440346567');
INSERT INTO `IMRelationShip` VALUES ('30', '20', '40', '0', '1440402230', '1440402230');
INSERT INTO `IMRelationShip` VALUES ('31', '28', '41', '0', '1440409134', '1440409134');
INSERT INTO `IMRelationShip` VALUES ('32', '34', '41', '0', '1440409399', '1440409399');
INSERT INTO `IMRelationShip` VALUES ('33', '2', '41', '0', '1440409987', '1440409987');
INSERT INTO `IMRelationShip` VALUES ('34', '37', '41', '0', '1440410353', '1440410353');
INSERT INTO `IMRelationShip` VALUES ('35', '33', '49', '0', '1440422606', '1440422606');
INSERT INTO `IMRelationShip` VALUES ('36', '43', '51', '0', '1440478718', '1440478718');
INSERT INTO `IMRelationShip` VALUES ('37', '12', '51', '0', '1440478766', '1440478766');
INSERT INTO `IMRelationShip` VALUES ('38', '3', '56', '0', '1440491662', '1440491662');
INSERT INTO `IMRelationShip` VALUES ('39', '28', '59', '0', '1440577613', '1440577613');
INSERT INTO `IMRelationShip` VALUES ('40', '41', '64', '0', '1440689900', '1440689900');
INSERT INTO `IMRelationShip` VALUES ('41', '64', '65', '0', '1440691549', '1440691549');

-- ----------------------------
-- Table structure for IMRole
-- ----------------------------
DROP TABLE IF EXISTS `IMRole`;
CREATE TABLE `IMRole` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pid` smallint(6) DEFAULT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `ename` varchar(5) DEFAULT NULL,
  `create_time` int(11) unsigned NOT NULL,
  `update_time` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`),
  KEY `ename` (`ename`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRole
-- ----------------------------
INSERT INTO `IMRole` VALUES ('12', '普通客户组', '0', '1', '普通客户组', null, '1438429901', '1438433156');
INSERT INTO `IMRole` VALUES ('13', '系统管理组', '0', '1', '系统管理组', null, '1438429918', '1439441719');

-- ----------------------------
-- Table structure for IMRoleUser
-- ----------------------------
DROP TABLE IF EXISTS `IMRoleUser`;
CREATE TABLE `IMRoleUser` (
  `role_id` mediumint(9) unsigned DEFAULT NULL,
  `user_id` char(32) DEFAULT NULL,
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of IMRoleUser
-- ----------------------------
INSERT INTO `IMRoleUser` VALUES ('4', '27');
INSERT INTO `IMRoleUser` VALUES ('4', '26');
INSERT INTO `IMRoleUser` VALUES ('4', '30');
INSERT INTO `IMRoleUser` VALUES ('5', '31');
INSERT INTO `IMRoleUser` VALUES ('3', '22');
INSERT INTO `IMRoleUser` VALUES ('1', '1');
INSERT INTO `IMRoleUser` VALUES ('1', '2');
INSERT INTO `IMRoleUser` VALUES ('2', '3');
INSERT INTO `IMRoleUser` VALUES ('7', '2');
INSERT INTO `IMRoleUser` VALUES ('3', '35');
INSERT INTO `IMRoleUser` VALUES ('3', '36');
INSERT INTO `IMRoleUser` VALUES ('7', '36');
INSERT INTO `IMRoleUser` VALUES ('3', '37');
INSERT INTO `IMRoleUser` VALUES ('3', '38');
INSERT INTO `IMRoleUser` VALUES ('3', '39');
INSERT INTO `IMRoleUser` VALUES ('1', '4');
INSERT INTO `IMRoleUser` VALUES ('3', '40');
INSERT INTO `IMRoleUser` VALUES ('2', '1');
INSERT INTO `IMRoleUser` VALUES ('2', '41');
INSERT INTO `IMRoleUser` VALUES ('12', '42');
INSERT INTO `IMRoleUser` VALUES ('12', '43');

-- ----------------------------
-- Table structure for IMUser
-- ----------------------------
DROP TABLE IF EXISTS `IMUser`;
CREATE TABLE `IMUser` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `sex` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '1男2女0未知',
  `name` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '用户名',
  `domain` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '拼音',
  `nick` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '花名,绰号等',
  `password` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(4) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '混淆码',
  `phone` varchar(11) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '手机号码',
  `email` varchar(64) COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT 'email',
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT '' COMMENT '自定义用户头像',
  `departId` int(11) unsigned NOT NULL COMMENT '所属部门Id',
  `status` tinyint(2) unsigned DEFAULT '0' COMMENT '1. 试用期 2. 正式 3. 离职 4.实习',
  `created` int(11) unsigned NOT NULL COMMENT '创建时间',
  `updated` int(11) unsigned NOT NULL COMMENT '更新时间',
  `mAvatar` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '缩略图地址400*400',
  PRIMARY KEY (`id`),
  KEY `idx_domain` (`domain`),
  KEY `idx_name` (`name`),
  KEY `idx_phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of IMUser
-- ----------------------------
INSERT INTO `IMUser` VALUES ('2', '2', 'weixiao', 'weixiao', '微笑哥哥', '6a6784920b5384997ce1f9ac554d9c65', '8020', '13723879918', '512720913@qq.com', '/Public/Uploads/Userimg/20150812/1439382041.jpg', '1', '0', '1439382041', '0', '/Public/Uploads/Userimg/20150812/m_1439382041.jpg');
INSERT INTO `IMUser` VALUES ('3', '2', 'suixxx', 'suixxx', '为什么这个不行', '73dff2d052bd69eb547d5dd9bac4a72d', '8211', '18073153190', '571378154@qq.com', '/Public/Uploads/Userimg/20150812/1439386557.jpg', '1', '0', '1439386557', '0', '/Public/Uploads/Userimg/20150812/m_1439386557.jpg');
INSERT INTO `IMUser` VALUES ('6', '2', 'issa', 'issa', 'issa888', 'f3333345c4bc7390fac88d8b8b5de00c', '2666', '13227020041', '132541753@163.com', '/Public/Uploads/Userimg/20150812/1439389730.jpg', '1', '0', '1439389731', '0', '/Public/Uploads/Userimg/20150812/m_1439389730.jpg');
INSERT INTO `IMUser` VALUES ('8', '2', 'mxbhxx', 'mxbhxx', 'mxbhxx', '51e307b62748e15d05f6a30f73770e39', '9915', '', 'ming537@126.com', '/Public/Uploads/Userimg/20150813/1439426853.png', '1', '0', '1439426853', '0', '/Public/Uploads/Userimg/20150813/m_1439426853.png');
INSERT INTO `IMUser` VALUES ('9', '2', 'seven_jobs', 'xiaobai', '小白', 'a0de41f8a2674d9d880cd7110ffbb9fe', '3179', '18651155863', 'luyangzou@163.com', '/Public/Uploads/Userimg/20150813/1439439066.png', '1', '0', '1439439066', '0', '/Public/Uploads/Userimg/20150813/m_1439439066.png');
INSERT INTO `IMUser` VALUES ('10', '2', 'liqk', 'pika2015', 'pika2015', '9eecb19845f5d66fcf1106b42ef0a2c7', '7617', '15813329327', '331201847@qq.com', '/Public/Uploads/Userimg/20150813/1439446094.png', '1', '0', '1439446094', '0', '/Public/Uploads/Userimg/20150813/m_1439446094.png');
INSERT INTO `IMUser` VALUES ('11', '2', 'tonyli001', 'onyi', 'TonyLi', '748cd679ceb49852f6cad6a111b51c65', '4444', '15989099516', '3223270386@qq.com', '/Public/Uploads/Userimg/20150813/1439455015.png', '1', '0', '1439455016', '0', '/Public/Uploads/Userimg/20150813/m_1439455015.png');
INSERT INTO `IMUser` VALUES ('12', '2', 'cjp472', 'chenjinping', '陈进平', '01d2b2f783a14bdb2d87e119527067c2', '9037', '13552247668', 'cjp472@163.com', '/Public/Uploads/Userimg/20150813/1439457838.jpg', '1', '0', '1439457838', '0', '/Public/Uploads/Userimg/20150813/m_1439457838.jpg');
INSERT INTO `IMUser` VALUES ('13', '2', 'xianbing', 'xianbing', 'xianbing', '846c52f4a77893dc985a5ee8326ce655', '1596', '18600955495', 'xianbing315@gmail.com', '/Public/Uploads/Userimg/20150813/1439474701.jpeg', '1', '0', '1439474701', '0', '/Public/Uploads/Userimg/20150813/m_1439474701.jpeg');
INSERT INTO `IMUser` VALUES ('14', '2', '50910018', '', '50910018', '720466ddec2acd7607df2a1cddc70f60', '7307', '18600169935', '50910018@qq.com', '/Public/Uploads/Userimg/20150813/1439475047.png', '1', '0', '1439475047', '0', '/Public/Uploads/Userimg/20150813/m_1439475047.png');
INSERT INTO `IMUser` VALUES ('15', '2', 'zhaoshan313', 'wuhuaguo', '无花果', '596f70a7bc6de8129fd026f72a6bb403', '1326', '18565871941', 'zhaoshan313@163.com', '/Public/Uploads/Userimg/20150814/1439538455.jpg', '1', '0', '1439538455', '0', '/Public/Uploads/Userimg/20150814/m_1439538455.jpg');
INSERT INTO `IMUser` VALUES ('16', '2', 'Lover103', 'over103', 'Lover103', 'f98072f096e19bf82f906fc50e5dd0a3', '6021', '13811111111', '454936716@qq.com', '/Public/Uploads/Userimg/20150814/1439540479.png', '1', '0', '1439540479', '0', '/Public/Uploads/Userimg/20150814/m_1439540479.png');
INSERT INTO `IMUser` VALUES ('17', '2', 'withparadox2', 'withparadox2', 'withparadox2', 'fbaa74540e0374e4fa7c583d2ffd6ca5', '3647', '15623919976', 'withparadox2@gmail.com', '/Public/Uploads/Userimg/20150814/1439540509.jpg', '1', '0', '1439540509', '0', '/Public/Uploads/Userimg/20150814/m_1439540509.jpg');
INSERT INTO `IMUser` VALUES ('18', '2', 'test_2015', 'kekoukele', '可口可乐', '5569ff55cea4f7a33f3a75425b05715c', '6579', '15275318218', 'fujunwei_1985@163.com', '/Public/Uploads/Userimg/20150814/1439541398.jpg', '1', '0', '1439541398', '0', '/Public/Uploads/Userimg/20150814/m_1439541398.jpg');
INSERT INTO `IMUser` VALUES ('19', '2', 'liboss', 'liboss', 'liboss', '7e81cdce56a9b09ea58c1bcc0434f390', '2009', '18001358078', 'hunan_lijie@163.com', '/Public/Uploads/Userimg/20150814/1439542222.jpg', '1', '0', '1439542222', '0', '/Public/Uploads/Userimg/20150814/m_1439542222.jpg');
INSERT INTO `IMUser` VALUES ('20', '2', 'liboss1', 'libosstest', 'liboss_test', '3cf1e40493e06753ee798c720375d85a', '6995', '13811457419', '303819511@qq.com', '/Public/Uploads/Userimg/20150814/1439542667.jpg', '1', '0', '1439542667', '0', '/Public/Uploads/Userimg/20150814/m_1439542667.jpg');
INSERT INTO `IMUser` VALUES ('21', '2', 'Axiu', 'axiu', 'axiu', '2801de36cf4e52c10c31a933b82f2579', '6344', '18810666366', '3150109140@qq.com', '/Public/Uploads/Userimg/20150814/1439543561.png', '1', '0', '1439543561', '0', '/Public/Uploads/Userimg/20150814/m_1439543561.png');
INSERT INTO `IMUser` VALUES ('22', '2', 'TEST001', 'axiu', 'axiu', '22baed1c08fbf383249e818f82f1593e', '8630', '13423434234', 'TEST@AA.COM', '/Public/Uploads/Userimg/20150814/1439543822.png', '1', '0', '1439543822', '0', '/Public/Uploads/Userimg/20150814/m_1439543822.png');
INSERT INTO `IMUser` VALUES ('23', '2', 'jianshuang', 'weixiaoceshizhanghu', '微笑测试账户', 'd94a84a79bbdc8d4bb3058be43944fc3', '8623', '13723879918', '13723879918@qq.com', '/Public/Uploads/Userimg//20150816/1439710018.jpg', '1', '0', '1439710018', '0', '/Public/Uploads/Userimg//m_20150816/1439710018.jpg');
INSERT INTO `IMUser` VALUES ('24', '2', 'icare_tt', 'icarett', 'icare_tt', '02db5693375988a1a0b340a89336ca83', '2966', '15267183070', 'cdhxcp@163.com', '/Public/Uploads/Userimg//20150818/1439860693.png', '1', '0', '1439860693', '0', '/Public/Uploads/Userimg//m_20150818/1439860693.png');
INSERT INTO `IMUser` VALUES ('25', '2', 'test2015', 'test', 'test', 'ce2f2a4e7825874637d76f2ca4d4ab3b', '2858', '', '1660747656@qq.com', '/Public/Uploads/Userimg//20150818/1439868031.png', '1', '0', '1439868031', '0', '/Public/Uploads/Userimg//m_20150818/1439868031.png');
INSERT INTO `IMUser` VALUES ('26', '2', 'panzebin', 'phigufdf5', 'phigufdf5', '0bcf5438466b96c5f2224ee719a3563b', '3253', '13411958151', '963470667@qq.com', '/Public/Uploads/Userimg//20150818/1439868134.jpg', '1', '0', '1439868134', '0', '/Public/Uploads/Userimg//m_20150818/1439868134.jpg');
INSERT INTO `IMUser` VALUES ('27', '2', 'test_test_123', '', 'TEST_TEST', 'c30e8bb715138aff86fa45a5c3264215', '5110', '', 'zulh@sciyon.com', '/Public/Uploads/Userimg//20150818/1439868464.jpg', '1', '0', '1439868464', '0', '/Public/Uploads/Userimg//m_20150818/1439868464.jpg');
INSERT INTO `IMUser` VALUES ('28', '2', 'test_test_124', '123456', 'TT123456', '2fa8257861974ebcaf833f794aa1e8a6', '3206', '', '86236578@qq.com', '/Public/Uploads/Userimg//20150818/1439868980.jpg', '1', '0', '1439868980', '0', '/Public/Uploads/Userimg//m_20150818/1439868980.jpg');
INSERT INTO `IMUser` VALUES ('29', '2', 'guideant', '', '', 'b86c1acbefd4603c2cd5cbc7dab476a1', '1008', '', 'Hawking_1992@hotmail.com', '/Public/Uploads/Userimg//20150818/1439869132.png', '1', '0', '1439869132', '0', '/Public/Uploads/Userimg//m_20150818/1439869132.png');
INSERT INTO `IMUser` VALUES ('30', '2', 'bran_guan', 'branguan', 'bran_guan', '1dff09baceb49978cc49d04ad57075ef', '9750', '15251528002', 'bran_guan@163.com', '/Public/Uploads/Userimg//20150818/1439883108.png', '1', '0', '1439883109', '0', '/Public/Uploads/Userimg//m_20150818/1439883108.png');
INSERT INTO `IMUser` VALUES ('31', '2', '123123', '', '213432423', '53b7ca9434e321155ce737b2d44f423b', '2732', '123123123', '123123@123.com', '/Public/Uploads/Userimg//20150819/1439921358.png', '1', '0', '1439921358', '0', '/Public/Uploads/Userimg//m_20150819/1439921358.png');
INSERT INTO `IMUser` VALUES ('32', '2', 'inspur', 'inspurxu', 'inspur_xu', 'd85adc18a29fdbcac2af36224035cf2a', '5944', '', 'xufeifei@inspur.com', '/Public/Uploads/Userimg//20150819/1439945766.png', '1', '0', '1439945766', '0', '/Public/Uploads/Userimg//m_20150819/1439945766.png');
INSERT INTO `IMUser` VALUES ('33', '2', 'abc_1234', 'abcdefg', 'abcdefg', 'b6fa4dfaa7611feabca01a6a1172186b', '9777', '', '111@163.com', '/Public/Uploads/Userimg//20150819/1439946729.png', '1', '0', '1439946729', '0', '/Public/Uploads/Userimg//m_20150819/1439946729.png');
INSERT INTO `IMUser` VALUES ('34', '2', 'chenc', 'chuangchuangchuangchuang', '闯闯闯闯', '25924b8711d527e22446c9c35da1b523', '4178', '', 'chch1028@163.com', '/Public/Uploads/Userimg/20150819/1439970565.jpg', '1', '0', '1439970565', '0', '');
INSERT INTO `IMUser` VALUES ('35', '2', 'chenchen', 'cccccccc', 'cccccccc', 'def5a0cc23fe61f771ad60af3430c0ce', '8091', '', 'chenc@sciyon.com', '/Public/Uploads/Userimg/20150819/1439971316.jpg', '1', '0', '1439971317', '0', '');
INSERT INTO `IMUser` VALUES ('36', '2', 'gaoming', 'yang', 'yang', '48bca9ed13aae222b31dff994f3701f7', '5141', '18867103593', '243457433@qq.com', '/Public/Uploads/Userimg/20150819/1439971721.png', '1', '0', '1439971721', '0', '');
INSERT INTO `IMUser` VALUES ('37', '2', 'chuang', 'cccccccc', 'cccccccc', '5b65584d0e567324c4a61e3a202008ed', '7387', '', 'c2610315@163.com', '/Public/Uploads/Userimg/20150819/1439971725.jpg', '1', '0', '1439971726', '0', '');
INSERT INTO `IMUser` VALUES ('38', '2', 'cyysun107', 'cyy90107', 'cyy90107', '56846f75354ee122c52cb50574f07292', '7001', '18701309695', 'cyysun107@163.com', '/Public/Uploads/Userimg/20150821/1440127398.jpg', '1', '0', '1440127399', '0', '');
INSERT INTO `IMUser` VALUES ('39', '2', 'zipeng', 'zipenghen', 'zipengChen', 'd5f6a4a4b278861dcd64a8f8ed48eff1', '2468', '15992668797', '453333957@qq.com', '/Public/Uploads/Userimg/20150821/1440167971.png', '1', '0', '1440167971', '0', '');
INSERT INTO `IMUser` VALUES ('40', '2', 'liboss2', 'liboss2', 'liboss2', 'd71410183e384a192bc4f3e62f322cd1', '7381', '13811690713', '475188472@qq.com', '/Public/Uploads/Userimg/20150824/1440401793.jpg', '1', '0', '1440401794', '0', '');
INSERT INTO `IMUser` VALUES ('41', '2', 'cqs5218669', 'xiaowuziya', 'xiaowuziya', '3280a1cd276aa12e3734b8cba27bd375', '3172', '18856025591', '31324315@qq.com', '/Public/Uploads/Userimg/20150824/1440408855.png', '1', '0', '1440408855', '0', '');
INSERT INTO `IMUser` VALUES ('42', '2', 'haha', 'wolegequ', '我勒个去', 'd1980da1009c800b3c823d89fa352348', '3519', '', 'haha@163.com', '/Public/Uploads/Userimg/20150824/1440409698.jpg', '1', '0', '1440409698', '0', '');
INSERT INTO `IMUser` VALUES ('43', '2', 'shen98li', 'shen98li', 'shen98li', '0b47952504ff194d054271aff2f992f0', '6607', '', 'shen98li@163.com', '/Public/Uploads/Userimg/20150824/1440409920.png', '1', '0', '1440409920', '0', '');
INSERT INTO `IMUser` VALUES ('44', '2', 'xhackertxl', 'weixiaobaqingchun', '微笑吧青春', '17917374eab02a7e12a49730b2521d72', '1432', '18656376889', '335682638@qq.com', '/Public/Uploads/Userimg/20150824/1440409959.png', '1', '0', '1440409959', '0', '');
INSERT INTO `IMUser` VALUES ('45', '2', 'dongwflj', 'hellott', 'hellott', 'e79f18cd98c3faa836d3576bb1f16d84', '4378', '13810488075', 'dongwflj@163.com', '/Public/Uploads/Userimg/20150824/1440412552.jpg', '1', '0', '1440412552', '0', '');
INSERT INTO `IMUser` VALUES ('46', '2', 'lecai', 'lecaisu', '乐彩苏', 'c6e4e353aa54dc69b3c42c296513803e', '1217', '18922805288', '464135859@qq.com', '/Public/Uploads/Userimg/20150824/1440419382.png', '1', '0', '1440419382', '0', '');
INSERT INTO `IMUser` VALUES ('47', '2', 'lecaiP', 'lecai', 'lecaiP', 'dd8d95d7a9b3a13a3f902fcf74dbec6b', '1267', '', 'jemmy.su.adai@gmail.com', '/Public/Uploads/Userimg/20150824/1440420312.png', '1', '0', '1440420312', '0', '');
INSERT INTO `IMUser` VALUES ('48', '2', 'hanyou', 'youge', '幽哥', '5ef93ff4c837c00b1cd61e371cf5e2d2', '6418', '13602651760', 'you.han@163.com', '/Public/Uploads/Userimg/20150824/1440420688.jpg', '1', '0', '1440420688', '0', '');
INSERT INTO `IMUser` VALUES ('49', '2', 'xiefeng', 'xiefng', 'xiefng', 'ae17a0a49cb6da7847eee4e22fb0857e', '7762', '18079738688', '814110852@qq.com', '/Public/Uploads/Userimg/20150824/1440421818.png', '1', '0', '1440421818', '0', '');
INSERT INTO `IMUser` VALUES ('50', '2', 'kdmmx', 'kdmmx', 'kdmmx', '34de6058ae8f73e15e5080f0a98e8623', '6662', '', 'shen98li@qq.com', '/Public/Uploads/Userimg/20150825/1440464803.png', '1', '0', '1440464803', '0', '');
INSERT INTO `IMUser` VALUES ('51', '2', 'xiaoma', 'xiaoma', 'xiaoma', 'faae2ba6ed3f7475728e653591f7d727', '3052', '15058960109', '3262215346@qq.com', '/Public/Uploads/Userimg/20150825/1440467264.jpg', '1', '0', '1440467264', '0', '');
INSERT INTO `IMUser` VALUES ('52', '2', 'veeson', 'veesonhuang', 'veeson huang', '09a6296cafe79cd6264d2b037e83d1db', '2460', '', 'veeson@homa.cn', '/Public/Uploads/Userimg/20150825/1440468737.png', '1', '0', '1440468737', '0', '');
INSERT INTO `IMUser` VALUES ('53', '2', 'zhaosong', 'laozhaolaozhao117', '老赵_laozhao_117', 'd70722b2d5d57230d02893b54d97a206', '9442', '18633896590', '18633896590@126.com', '/Public/Uploads/Userimg/20150825/1440469132.png', '1', '0', '1440469132', '0', '');
INSERT INTO `IMUser` VALUES ('54', '2', 'test', 'test', 'test', '6a0d54f35ada170c3c7fe91d23a821ff', '6855', '11111111111', 'test@123.com', '/Public/Uploads/Userimg/20150825/1440486098.png', '1', '0', '1440486098', '0', '');
INSERT INTO `IMUser` VALUES ('55', '2', 'webster', 'hahaha', '哈哈哈____', '3dd76489c9e151962bc52f1a5a7323c2', '5348', '', 'lostever2013@163.com', '/Public/Uploads/Userimg/20150825/1440486617.jpg', '1', '0', '1440486617', '0', '');
INSERT INTO `IMUser` VALUES ('56', '2', 'jianxuzhang', 'jianxuzhang', 'jianxuzhang', 'aa0698aa4406169129f3b5ea0d25118d', '8954', '13564219265', 'jianxuzhang@163.com', '/Public/Uploads/Userimg/20150825/1440491244.png', '1', '0', '1440491244', '0', '');
INSERT INTO `IMUser` VALUES ('57', '2', 'zeroyou', 'peter', 'peter', 'fa80fcbd0c52a6186d0834120b505534', '4446', '', 'zeroyou@163.com', '/Public/Uploads/Userimg/20150825/1440493432.png', '1', '0', '1440493432', '0', '');
INSERT INTO `IMUser` VALUES ('58', '2', 'wr450702', 'xuehanfeng', '薛旱枫', 'fffbb7621a47a3b3f467812210345a30', '8435', '15296339529', '554781433@qq.com', '/Public/Uploads/Userimg/20150825/1440495382.png', '1', '0', '1440495382', '0', '');
INSERT INTO `IMUser` VALUES ('59', '2', 'aoe1', 'aoetest1', 'aoetest1', '4026c80e07b3d894e44b6e873840f669', '9363', '', 'henrik@aoetech.cn', '/Public/Uploads/Userimg/20150826/1440560430.png', '1', '0', '1440560430', '0', '');
INSERT INTO `IMUser` VALUES ('60', '2', 'aoe2', 'aoetest2', 'aoetest2', 'a239b98852b519b25d6bcfa923f6dcf2', '5816', '', 'henrik2@aoetech.cn', '/Public/Uploads/Userimg/20150826/1440560478.png', '1', '0', '1440560478', '0', '');
INSERT INTO `IMUser` VALUES ('61', '2', 'xj211031', 'xj211031', 'xj211031', '1f8aacd2ba7f4612f986386891413ee0', '8322', '', 'j10y@163.com', '/Public/Uploads/Userimg/20150826/1440567416.jpg', '1', '0', '1440567416', '0', '');
INSERT INTO `IMUser` VALUES ('62', '2', 'MingGG', 'ing', 'Ming', 'd5b5fb0b590e9fbffa57679a6ea27ead', '9359', '18867103593', '89758237@qq.com', '/Public/Uploads/Userimg/20150826/1440568647.jpg', '1', '0', '1440568647', '0', '');
INSERT INTO `IMUser` VALUES ('63', '2', 'xinyilan', 'xinyi', 'xinyi', '27a32b2f9261cbb2028468c79871c888', '5235', '18220681168', '245503302@qq.com', '/Public/Uploads/Userimg/20150826/1440577591.jpg', '1', '0', '1440577591', '0', '');
INSERT INTO `IMUser` VALUES ('64', '2', 'cqs0048xlx', 'xiaowuziya001', 'xiaowuziya001', '675af6c4e522274dfaa80debbc2849af', '1804', '18856025591', '643085785@qq.com', '/Public/Uploads/Userimg/20150827/1440689420.png', '1', '0', '1440689421', '0', '');
INSERT INTO `IMUser` VALUES ('65', '2', 'wq123', 'wqbaobeia', 'wqbaobeia', '7afd3ef868a887779ee1333a61748740', '8946', '13716505149', '359288649@qq.com', '/Public/Uploads/Userimg/20150827/1440690461.png', '1', '0', '1440690461', '0', '');
