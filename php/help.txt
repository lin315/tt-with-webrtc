蓝狐博客地址：http://bluefoxah.org/teamtalk/new_tt_deploy.html
微笑论坛地址：http://bbs.p52.cn

快速安装php环境部分
yum -y install php php-mysql php-gd php-imap php-ldap php-odbc php-pear php-xml php-xmlrpc php-mbstring

关闭SELINUX（重启服务器）
编辑/etc/selinux/config，找到SELINUX 行修改成为：SELINUX=disabled:

建议打开部分端口
临时性的完全关闭防火墙，可以不重启机器：
#/etc/init.d/iptables status                ## 查看防火墙状态
#/etc/init.d/iptable stop                    ## 本次关闭防火墙
#/etc/init.d/iptable restart                ## 重启防火墙
永久性关闭防火墙：
#chkconfig --level 35 iptables off   ## 注意中间的是两个英式小短线；重启

配置apache 支持伪静态
打开apache的配置文件httpd.conf
找到
#LoadModule rewrite_module modules/mod_rewrite.so
把前面#去掉。没有则添加，但必选独占一行，使apache支持 mod_rewrite 模块

找到 AllowOverride None 换成 AllowOverride All 使apache支持 .htaccess 文件
<Directory "/var/www/html">
    #
    # Possible values for the Options directive are "None", "All",
    # or any combination of:
    #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
    #
    # Note that "MultiViews" must be named *explicitly* --- "Options All"
    # doesn't give it to you.
    #
    # The Options directive is both complicated and important.  Please see
    # http://httpd.apache.org/docs/2.4/mod/core.html#options
    # for more information.
    #
    Options Indexes FollowSymLinks

    #
    # AllowOverride controls what directives may be placed in .htaccess files.
    # It can be "All", "None", or any combination of the keywords:
    #   Options FileInfo AuthConfig Limit
    #
    AllowOverride All

    #
    # Controls who can get stuff from this server.
    #
    Require all granted
</Directory> 


