<?php if (!defined('THINK_PATH')) exit();?><div class="page">
	<div class="pageContent">

	<form method="post" action="__URL__/update/navTabId/__MODULE__" class="pageForm required-validate" onsubmit="return validateCallback(this, dialogAjaxMenu)">
		<input type="hidden" name="id" value="<?php echo ($vo["id"]); ?>" >
		<div class="pageFormContent" layoutH="58">
			<div class="unit">
				<label>导航：</label>
			<select name="group_menu">
					<?php if(is_array($groupClass)): $i = 0; $__LIST__ = $groupClass;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$g): $mod = ($i % 2 );++$i;?><option value="<?php echo ($g["menu"]); ?>" <?php if(($vo["group_menu"]) == $g["menu"]): ?>selected=selected<?php endif; ?>><?php echo ($g["name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
				</select>
					</div>
			<div class="unit">
				<label>标识：</label>
				<input type="text" class="required alphanumeric" value="<?php echo ($vo["name"]); ?>"  name="name">
			</div>
			
			<div class="unit">
				<label>显示名称：</label>
				<input type="text" class="required"  value="<?php echo ($vo["title"]); ?>"  name="title">
			</div>
			<div class="unit">
				<label>状态：</label>
				<SELECT name="status">
					<option value="1" <?php if(($vo["status"]) == "1"): ?>selected<?php endif; ?>>启用</option>
					<option value="0" <?php if(($vo["status"]) == "0"): ?>selected<?php endif; ?>>禁用</option>
				</SELECT>
			</div>
			<div class="unit">
				<label>顺序：</label>
				<input   name="sort" type="text" class="required" value="<?php echo ($vo["sort"]); ?>" size="5"/>
			</div>
		</div>

		<div class="formBar">
			<ul>
				<li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
				<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
			</ul>
		</div>
	</form>
	
	</div>
</div>